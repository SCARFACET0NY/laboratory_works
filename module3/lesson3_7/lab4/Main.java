package com.brainacad.module3.lesson3_7.lab4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        List<Integer> output = Stream.iterate(10, n -> n + 10)
                                        .limit(10)
                                        .map(n -> n / 2)
                                        .collect(Collectors.toList());
        System.out.println(output);

        List<String> strings = Arrays.asList(
                "java", "scala", "spring", "spark", "hibernate", "play", "vaadin", "akka", "hadoop", "kafka");
        strings.stream()
                .filter(s -> s.startsWith("s"))
                .sorted()
                .forEach(System.out::println);
        System.out.println();

        List<Person> persons = new ArrayList<>();
        persons.add(new Person("Dandellion", 27, "male"));
        persons.add(new Person("Cirilla", 19, "female"));
        persons.add(new Person("Gerald", 43, "male"));
        persons.add(new Person("Yennifer", 34, "female"));
        persons.add(new Person("Gaunter", 18, "male"));

        persons.stream()
                .filter(person -> person.getAge() >= 18 && person.getAge() <= 27)
                .filter(person -> person.getGender().equals("male"))
                .forEach(System.out::println);
        System.out.println();

        double avgAge = persons.stream()
                .filter(person -> person.getGender().equals("female"))
                .mapToInt(Person::getAge)
                .average()
                .getAsDouble();
        System.out.println(avgAge);
    }
}
