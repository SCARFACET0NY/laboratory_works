package com.brainacad.module3.lesson3_7.lab1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Integer[] nums = new Integer[10];
        for (int i = 0; i < nums.length; i++) {
            nums[i] = (int)(Math.random() * 100);
        }

        System.out.println(Arrays.toString(nums));
        Arrays.sort(nums, (n1, n2) -> (n2 - n1));
        System.out.println(Arrays.toString(nums));
        System.out.println();

        List<String> strings = new ArrayList<>();
        strings.add("Anton");
        strings.add("Bobby");
        strings.add("Christopher");
        strings.add("Daniel");
        strings.add("Frank");

        System.out.println(strings);
        Collections.sort(strings, (s1, s2) -> s2.compareTo(s1));
        System.out.println(strings);
    }
}
