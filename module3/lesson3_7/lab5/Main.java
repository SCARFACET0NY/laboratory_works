package com.brainacad.module3.lesson3_7.lab5;

import java.time.*;
import java.time.format.DateTimeFormatter;

public class Main {
    public static void main(String[] args) {
        LocalDate today = LocalDate.now();
        LocalDate birthday = LocalDate.of(1986, Month.MARCH, 20);
        int years = birthday.until(today).getYears();
        DayOfWeek birthdayDayYearBorn = birthday.getDayOfWeek();
        DayOfWeek birthdayDayThisYear = birthday.plusYears(31).getDayOfWeek();
        boolean wasBirthday = Period.between(today, LocalDate.of(2017, 3, 20)).getDays() <= 0;

        LocalDateTime time1 = LocalDateTime.ofInstant(Instant.now(), ZoneId.of("Europe/Kiev"));
        LocalDateTime time2 = LocalDateTime.ofInstant(Instant.now(), ZoneId.of("America/New_York"));
        LocalDateTime time3 = LocalDateTime.ofInstant(Instant.now(), ZoneId.of("Asia/Tokyo"));
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd.MM.yyyy, HH-mm-ss");
        String formattedTime1 = time1.format(format);
        String formattedTime2 = time2.format(format);
        String formattedTime3 = time3.format(format);


        System.out.println(today);
        System.out.println(birthday);
        System.out.println(years);
        System.out.println(birthdayDayYearBorn);
        System.out.println(birthdayDayThisYear);
        System.out.println(wasBirthday);
        System.out.println(formattedTime1);
        System.out.println(formattedTime2);
        System.out.println(formattedTime3);
    }
}
