package com.brainacad.module3.lesson3_7.lab2_3;

@FunctionalInterface
public interface MyConverter {
    String convertStr(String str);

    static boolean isNull(String str) {
        return str == null;
    }
}
