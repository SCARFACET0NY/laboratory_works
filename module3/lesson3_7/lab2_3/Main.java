package com.brainacad.module3.lesson3_7.lab2_3;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class Main {
    public static Integer sumEven(Integer[] arr, Predicate<Integer> pred) {
        int sum = 0;
        for (int i : arr) {
            if (pred.test(i)) sum += i;
        }
        return sum;
    }

    public static void printJStr(List<String> list, Predicate<String> pred) {
        for (String str : list) {
            if (!MyConverter.isNull(str) && pred.test(str)) {
                System.out.print(str + " ");
            }
        }
    }

    public static void updateList(List<String> list) {
        MyConverter mc = x -> MyConverter.isNull(x) ? null : x.toUpperCase();
        list.forEach(s -> list.set(list.indexOf(s), mc.convertStr(s)));
    }

    public static void main(String[] args) {
        Integer[] nums = {1, 2, 3, 4, 5, 6, 7, 8};
        System.out.println(sumEven(nums, x -> x % 2 == 0));

        List<String> list = Arrays.asList("car", "dog", "Cat", "table", "window", null);

        printJStr(list, x -> (x.toLowerCase().startsWith("c")));
        System.out.println();

        System.out.println(list);
        updateList(list);
        System.out.println(list);
    }
}
