package com.brainacad.module3.lesson3_5.lab1;

public class MyClass {
    private int a;
    int b;
    protected int c;
    public int d;

    public MyClass() {

    }

    public MyClass(int a) {
        this.a = a;
    }

    public MyClass(int a, int b, int c, int d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }
}
