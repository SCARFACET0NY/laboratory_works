package com.brainacad.module3.lesson3_5.lab1;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class Main {
    public static void main(String[] args) {
        MyClass c = new MyClass(1, 2, 3, 4);
        Class<?> cls = c.getClass();

        System.out.println("Class name: " + cls.getCanonicalName());
        int i = cls.getModifiers();
        System.out.printf("Modifiers:%n%s%n", Modifier.toString(i));

        System.out.println("Public fields:");
        Field[] pulicFields = cls.getFields();
        for (Field field : pulicFields) {
            Class<?> fType = field.getType();
            System.out.println("\tName: " + field.getName());
            System.out.println("\tType: " + fType.getName());
        }

        System.out.println("All fields:");
        Field[] allFields = cls.getDeclaredFields();
        for (Field field : allFields) {
            Class<?> fType = field.getType();
            System.out.println("\tName: " + field.getName());
            System.out.println("\tType: " + fType.getName());
        }

        System.out.println("Constructors:");
        Constructor<?>[] constructors = cls.getConstructors();
        int j = 0;
        for (Constructor constructor : constructors) {
            System.out.print("Constructor " + (j++) + "; ");
            Class<?>[] paramTypes = constructor.getParameterTypes();
            for (Class<?> paramType : paramTypes) {
                System.out.print(paramType.getName() + " ");
            }
            System.out.println();
        }

        System.out.println("Methods:");
        Method[] methods = cls.getDeclaredMethods();
        for (Method method : methods) {
            System.out.println("Name: " + method.getName());
            System.out.println("\tReturn type: " + method.getReturnType());
            Class<?>[] paramTypes = method.getParameterTypes();
            System.out.print("\tParam types:");
            for (Class<?> paramType : paramTypes) {
                System.out.print(" " + paramType.getName());
            }
            System.out.println();
        }
    }
}
