package com.brainacad.module3.lesson3_5.lab2;

import java.lang.reflect.Field;

public class Main {
    public static void main(String[] args) {
        try {
            String myStr = "abcd";
            Field field = String.class.getDeclaredField("value");
            field.setAccessible(true);
            System.out.println("Old string value: " + myStr);
            field.set(myStr, "zxcv".toCharArray());
            System.out.println("New string value: " + myStr);
            System.out.println("abcd");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
