package com.brainacad.module3.lesson3_5.lab3;

import com.brainacad.module3.lesson3_5.lab1.MyClass;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) {
        try {
            Class<?> cls = Class.forName("com.brainacad.module3.lesson3_5.lab2.MyClass");
            Constructor constructor = cls.getDeclaredConstructor(int.class);
            constructor.setAccessible(true);
            MyClass myClass = (MyClass)constructor.newInstance(5);

            Class<?>[] paramTypes = new Class<?>[]{int.class};
            Method method = cls.getDeclaredMethod("setA", paramTypes);
            method.invoke(myClass, 33);

            System.out.println("All fields:");
            Field[] allFields = cls.getDeclaredFields();
            for (Field field : allFields) {
                Class<?> fType = field.getType();
                field.setAccessible(true);
                System.out.println("\tName: " + field.getName());
                System.out.println("\t\tType: " + field.getName());
                System.out.println("\t\tValue: " + field.get(myClass));
            }
        } catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace();
        } catch (InstantiationException ie) {
            ie.printStackTrace();
        } catch (IllegalAccessException iae) {
            iae.printStackTrace();
        } catch (InvocationTargetException x) {
            x.printStackTrace();
        } catch (NoSuchMethodException nsme) {
            nsme.printStackTrace();
        }
    }
}
