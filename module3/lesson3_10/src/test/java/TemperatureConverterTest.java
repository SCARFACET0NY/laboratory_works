import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TemperatureConverterTest {
    TemperatureConverter converter;

    @Before
    public void init() {
        converter = new TemperatureConverter();
    }

    @Test
    public void testConvertFtoC() {
        assertEquals(5, converter.convertFtoC(41), 0.1);
    }

    @Test
    public void testConvertCtoF() {
        assertEquals(50, converter.convertCtoF(10), 0.1);
    }

    @Test
    public void testConvertCtoK() {
        assertEquals(250, converter.convertCtoK(-23.2), 0.1);
    }

    @Test
    public void testConvertKtoC() {
        assertEquals(-173.2, converter.convertKtoC(100), 0.1);
    }

    @Test
    public void testConvertFtoK() {
        assertEquals(250, converter.convertFtoK(-9.7), 0.1);
    }

    @Test
    public void testConvertKtoF() {
        assertEquals(-423.7, converter.convertKtoF(20), 0.1);
    }
}
