package com.brainacad.module3.lesson3_9.lab1;

import java.lang.reflect.Proxy;

public class Main {
    public static void main(String[] args) {
        Calculate calculate = (Calculate)Proxy.newProxyInstance(
                CalculateImpl.class.getClassLoader(),
                CalculateImpl.class.getInterfaces(),
                CalculateProxy.newInstance(new CalculateImpl())
        );

        calculate.multiplication(5, 2);
        calculate.division(5, 2);
    }
}
