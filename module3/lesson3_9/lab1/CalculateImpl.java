package com.brainacad.module3.lesson3_9.lab1;

public class CalculateImpl implements Calculate {
    @Override
    public double multiplication(double a, double b) {
        return a * b;
    }

    @Override
    public double division(double a, double b) {
        return a / b;
    }
}
