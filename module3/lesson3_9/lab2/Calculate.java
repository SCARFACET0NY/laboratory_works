package com.brainacad.module3.lesson3_9.lab2;

public interface Calculate {
    double multiplication(double a, double b);
    double division(double a, double b);
}
