package com.brainacad.module3.lesson3_9.lab2;

public class CalculateBitwiseImpl implements CalculateBitwise {
    @Override
    public int andBitwise(int a, int b) {
        return a & b;
    }

    @Override
    public int orBitwise(int a, int b) {
        return a | b;
    }
}
