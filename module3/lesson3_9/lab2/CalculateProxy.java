package com.brainacad.module3.lesson3_9.lab2;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.List;

public class CalculateProxy implements InvocationHandler {
    private Class<?>[] interfaces;
    private Object[] delegates;

    private CalculateProxy(Class[] classes, Object[] targets) {
        interfaces = classes;
        delegates = targets;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Class<?> declaredClass = method.getDeclaringClass();
        for (int i = 0; i < interfaces.length; i++) {
            if (declaredClass.isAssignableFrom(interfaces[i])) {
                System.out.println("The method " + method.getDeclaringClass() + " invoked with args: " + Arrays.toString(args));
                Object result = method.invoke(delegates[i], args);
                System.out.println("The method " + method.getDeclaringClass() + " ends with result " + result.toString());
                return result;
            }
        }
        System.out.println("Nothing found");
        return null;
    }

    public static Object newInstance(Class interfaceClass) {
        Object[] delegates = new Object[] {new CalculateImpl(), new CalculateBitwiseImpl()};
        //List<Class<?>> delegates = Arrays.asList(interfaceClass.getClasses());
        //List<Class<?>> interfaces = Arrays.asList(interfaceClass.getInterfaces());
        List<Class> interfaces = Arrays.asList(Calculate.class, CalculateBitwise.class);

        if (interfaces.contains(interfaceClass)) {
            Class[] proxyInterfaces = interfaces.toArray(new Class[interfaces.size()]);
            return Proxy.newProxyInstance(
                    interfaceClass.getClassLoader(),
                    proxyInterfaces,
                    new CalculateProxy(proxyInterfaces, delegates));
        }
        System.out.println("Can not create object!");
        return  null;
//        List<Class<?>> proxyInterfaces = Arrays.asList(
//                Proxy.getProxyClass(interfaceClass.getClassLoader(), interfaces).getInterfaces());
//        Object object = null;
//        try {
//            object = interfaceClass.newInstance();
//        } catch (InstantiationException | IllegalAccessException ie) {
//            ie.printStackTrace();
//        }
//        return proxyInterfaces.contains(interfaceClass) ?
//                new CalculateProxy(interfaces, delegates)
//                : null;
    }
}
