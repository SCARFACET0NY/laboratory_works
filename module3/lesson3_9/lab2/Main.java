package com.brainacad.module3.lesson3_9.lab2;

import java.lang.reflect.Proxy;

public class Main {
    public static void main(String[] args) {
        Calculate calculate1 = (Calculate)CalculateProxy.newInstance(Calculate.class);
        CalculateBitwise calculate2 = (CalculateBitwise)CalculateProxy.newInstance(CalculateBitwise.class);

        calculate1.multiplication(5, 2);
        calculate1.division(5, 2);
        calculate2.andBitwise(5, 2);
        calculate2.orBitwise(5, 2);
    }
}
