package com.brainacad.module3.lesson3_9.lab2;

public interface CalculateBitwise {
    int andBitwise(int a, int b);
    int orBitwise(int a, int b);
}
