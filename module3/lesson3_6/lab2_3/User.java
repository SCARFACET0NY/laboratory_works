package com.brainacad.module3.lesson3_6.lab2_3;

import java.util.List;

enum PermissionAction {
    USER_READ, USER_CHANGE, USER_WRITE
}

public class User {
    private String name;
    private List<PermissionAction> permissions;

    public User(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "User name = '" + name;
    }

    public List<PermissionAction> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<PermissionAction> permissions) {
        this.permissions = permissions;
    }
}
