package com.brainacad.module3.lesson3_6.lab2_3;

import java.io.*;
import java.lang.reflect.Method;

public class Action {
    @MyPermission(PermissionAction.USER_CHANGE)
    public void write(User user, String text) {
        if (checkPermissions(PermissionAction.USER_CHANGE, user)) {
            try (
                    FileOutputStream fos = new FileOutputStream("file.txt");
                    PrintWriter writer = new PrintWriter(fos);
            ) {
                writer.println(text);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        } else {
            System.out.println("Access forbidden");
        }
    }

    @MyPermission(PermissionAction.USER_READ)
    public void read(User user) {
        if (checkPermissions(PermissionAction.USER_READ, user)) {
            try (
                    FileInputStream fis = new FileInputStream("file.txt");
                    BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
            ) {
                String line;
                while ((line = reader.readLine()) != null) {
                    System.out.println(line);
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        } else {
            System.out.println("Access forbidden");
        }
    }

    public static boolean checkPermissions (PermissionAction permission, User user) {
        for (PermissionAction action : user.getPermissions()) {
            if (permission == action) {
                return true;
            }
        }

        return false;
    }
}
