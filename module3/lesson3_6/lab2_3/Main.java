package com.brainacad.module3.lesson3_6.lab2_3;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Action action = new Action();
        User admin = new User("John");
        User user = new User("Jack");

        List<PermissionAction> list1 = new ArrayList<>();
        List<PermissionAction> list2 = new ArrayList<>();
        list1.add(PermissionAction.USER_CHANGE);
        list1.add(PermissionAction.USER_READ);
        list2.add(PermissionAction.USER_READ);
        admin.setPermissions(list1);
        user.setPermissions(list2);

        action.write(admin, "Wrote as admin");
        action.read(admin);
        action.write(user, "Wrote as user");
        action.read(user);
    }
}
