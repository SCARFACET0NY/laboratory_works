package com.brainacad.module3.lesson3_6.lab1;

import java.util.Arrays;

public class Main {
    /**
     * @deprecated
     * just tired of same old implementation
     */
    @Deprecated
    public static int findMax(int... nums) {
        int max = Integer.MIN_VALUE;
        for (int num : nums) {
            if (num > max) max = num;
        }
        return nums.length > 0 ? max : 0;
    }
    @SafeVarargs
    public static <T> T findMaxG(T... nums) {
        T max;
        Arrays.sort(nums);
        max = nums[nums.length - 1];
        return max;
    }

    @SuppressWarnings("deprecation")
    public static void main(String[] args) {
        System.out.println(Main.findMax(-5, 12, 48, 3));
        System.out.println(Main.findMaxG(-5.0, 12.0, 48.0, 3.0));
    }
}
