package com.brainacad.module3.lesson3_4.lab1_2_3;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;

public class MyApplication extends JFrame {
    private JLabel label;
    private JComboBox list;
    private JButton button;

    public MyApplication(String name) throws HeadlessException {
        super(name);
        this.setSize(500, 400);
        this.setLocation(300, 150);
        this.setLayout(new FlowLayout());
        this.createControlsRun();
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent windowEvent){
                System.exit(0);
            }
        });
        this.setVisible(true);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public void createControlsRun() {
        label = new JLabel("Run a Program");
        list = new JComboBox(new String[] {"Calc", "Notepad", "MsPaint"});
        button = new JButton("Run");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Runtime.getRuntime().exec(list.getSelectedItem().toString().toLowerCase() + ".exe");
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        });
        this.add(label);
        this.add(list);
        this.add(button);
    }
}
