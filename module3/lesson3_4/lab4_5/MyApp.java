package com.brainacad.module3.lesson3_4.lab4_5;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class MyApp extends JFrame {
    public String computerName;
    private JMenuBar menuBar;
    private JMenu menu;
    public static JTextField textField;

    public MyApp(String name) throws HeadlessException {
        super(name);
        this.setSize(400, 200);
        this.setLocation(300, 150);
        this.setLayout(new BorderLayout());
        this.createMenu();
        this.createComputerName();
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent windowEvent){
                System.exit(0);
            }
        });
        this.setVisible(true);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public void createComputerName() {
        JPanel panelTop = new JPanel();
        JTextField set = new JTextField(20);
        JButton setName = new JButton("Set User Name");
        setName.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               computerName = set.getText() + "-DESKTOP";
            }
        });

        JPanel panelBottom = new JPanel();
        JTextField get = new JTextField(20);
        JButton getName = new JButton("Get User Name");
        getName.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                get.setText(computerName);
            }
        });

        panelTop.add(set);
        panelTop.add(setName);
        panelBottom.add(get);
        panelBottom.add(getName);

        this.add(panelTop, BorderLayout.NORTH);
        this.add(panelBottom, BorderLayout.SOUTH);
    }

    public void createMenu() {
        JPanel panelCenter = new JPanel();
        JLabel label = new JLabel("Result");
        textField = new JTextField(30);

        panelCenter.add(label);
        panelCenter.add(textField);
        this.add(panelCenter, BorderLayout.CENTER);

        menuBar = new JMenuBar();
        this.setJMenuBar(menuBar);
        menu = new JMenu("Message");
        JMenuItem name = new JMenuItem("Input Name");
        name.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new MyDialog(new JFrame(), "Name", "Enter the user name");
            }
        });
        JMenuItem question = new JMenuItem("Question");
        question.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new MyDialog(new JFrame(), "Question", "Are you ready to undergo the test");
            }
        });

        menu.add(name);
        menu.add(question);
        menuBar.add(menu);
    }
}

class MyDialog extends JDialog {
    private final JFrame parent;
    private JTextField field;
    private JLabel label;
    private JButton button;

    public MyDialog(final JFrame parent, String name, String labelName) {
        super(parent, name);
        this.parent = parent;
        this.setSize(300, 150);
        this.setLocation(350, 200);
        this.setLayout(new FlowLayout());

        label = new JLabel(labelName);
        field = new JTextField(20);
        button = new JButton("OK");

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MyApp.textField.setText(field.getText());
                parent.dispose();
            }
        });
        this.add(label);
        this.add(field);
        this.add(button);

        this.setVisible(true);
        this.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
    }
}
