package com.brainacad.module3.lesson3_2.lab4;

import java.io.*;

public class Main {
    public static void main(String[] args) {
        User user = new User();
        try (
                RandomAccessFile users = new RandomAccessFile("users.ser", "rw");
                FileOutputStream fos = new FileOutputStream(users.getFD());
                ObjectOutputStream oos = new ObjectOutputStream(fos);
        ){
            oos.writeObject(user);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        try (
                FileInputStream fis = new FileInputStream("users.ser");
                ObjectInputStream ois = new ObjectInputStream(fis);
        ){

            user = (User)ois.readObject();
            System.out.println(user.toString());
        } catch(ClassNotFoundException cnfe) {
            cnfe.printStackTrace();
        } catch (IOException ioe){
            ioe.printStackTrace();
        }

//        User user = new User();
//        int length = user.toString().length();
//        try (
//                RandomAccessFile users = new RandomAccessFile("users.ser", "rw");
//                FileOutputStream fos = new FileOutputStream(users.getFD());
//                ObjectOutputStream oos = new ObjectOutputStream(fos);
//        ){
//            oos.writeBytes(user.toString());
//        } catch (IOException ioe) {
//            ioe.printStackTrace();
//        }
//
//        try (
//                RandomAccessFile users = new RandomAccessFile("users.ser", "rw");
//                FileInputStream fis = new FileInputStream(users.getFD());
//                ObjectInputStream ois = new ObjectInputStream(fis);
//        ){
//            for (int i = 0; i < length ; i++) {
//                System.out.print("" + (char) ois.readByte());
//            }
//        } catch (IOException ioe){
//            ioe.printStackTrace();
//        }
    }
}
