package com.brainacad.module3.lesson3_2.lab5;

import java.io.*;

public class Main {
    public static void main(String[] args) {
        User[] users = {
                new User(),
                new User("T-800", "Terminator", 35),
                new User("T-1000", "Terminator", 25)};
        try (
                RandomAccessFile file = new RandomAccessFile("users.ser", "rw");
                FileOutputStream fos = new FileOutputStream(file.getFD());
                ObjectOutputStream oos = new ObjectOutputStream(fos);
        ){
            oos.writeObject(users);
            oos.flush();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        try (
                FileInputStream fis = new FileInputStream("users.ser");
                ObjectInputStream ois = new ObjectInputStream(fis);
        ){
            users = (User[])ois.readObject();
            for (User user : users) {
                System.out.println(user.toString());
            }
        } catch(ClassNotFoundException cnfe) {
            cnfe.printStackTrace();
        } catch (IOException ioe){
            ioe.printStackTrace();
        }
    }
}
