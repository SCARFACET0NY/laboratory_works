package com.brainacad.module3.lesson3_2.lab1;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SerializeDemo {
    public static void main(String[] args) {
        Employee employee = new Employee("Anton", "Kiev", 123456, 25);

        try (
                FileOutputStream fos = new FileOutputStream("employee.ser");
                ObjectOutputStream oos = new ObjectOutputStream(fos);
        ){
            oos.writeObject(employee);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
