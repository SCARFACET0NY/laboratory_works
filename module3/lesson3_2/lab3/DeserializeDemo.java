package com.brainacad.module3.lesson3_2.lab3;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DeserializeDemo {
    public static void main(String[] args) {
        Employee employee;
        try (
                FileInputStream fis = new FileInputStream("employee.ser");
                ObjectInputStream ois = new ObjectInputStream(fis);
        ){
            employee = (Employee)ois.readObject();
            System.out.println(employee.toString());
        } catch(ClassNotFoundException cnfe) {
            cnfe.printStackTrace();
        } catch (IOException ioe){
            ioe.printStackTrace();
        }
    }
}
