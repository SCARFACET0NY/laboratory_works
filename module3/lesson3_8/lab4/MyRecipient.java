package com.brainacad.module3.lesson3_8.lab4;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

public class MyRecipient {
    private File file = new File("javaSE.jpg");

    public void run() {
        byte[] buffer = new byte[1];
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
        try (
                FileOutputStream out = new FileOutputStream(file);
                DatagramSocket server = new DatagramSocket(4444)
            ) {
            server.setSoTimeout(7000);
            System.out.println("Starting receiving data...");
            while (true) {
                server.receive(packet);
                out.write(buffer);
                out.flush();
            }
        } catch (SocketException se) {
            se.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public static void main(String[] args) {
        MyRecipient recipient = new MyRecipient();
        recipient.run();
    }
}
