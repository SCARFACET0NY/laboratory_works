package com.brainacad.module3.lesson3_8.lab4;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.*;

public class MySender {
    private File file;

    public MySender(String path) {
        this.file = new File(path);
    }

    public void run() {
        byte[] buffer = new byte[1];
        DatagramPacket packet = null;
        try (
                FileInputStream in = new FileInputStream(file);
                DatagramSocket server = new DatagramSocket()
            ) {
            InetAddress ip = InetAddress.getLocalHost();
            System.out.println("Starting sending data...");
            while (in.read(buffer) != -1) {
                packet = new DatagramPacket(buffer, buffer.length, ip, 4444);
                server.send(packet);
            }
            System.out.println("Sending complete");
        } catch (FileNotFoundException fnfe) {
            fnfe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public static void main(String[] args) {
        MySender sender = new MySender("Java.jpg");
        sender.run();
    }
}
