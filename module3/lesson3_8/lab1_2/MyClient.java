package com.brainacad.module3.lesson3_8.lab1_2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class MyClient extends Thread {
    private Student student;

    public MyClient(Student student) {
        this.student = student;
    }

    @Override
    public void run() {
        try (
                Socket socket = new Socket("localhost", 2222);
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()))
        ) {
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject(student);
            oos.flush();
            System.out.println(in.readLine());
            in.close();
            oos.close();
            socket.setSoTimeout(1000);
            socket.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
