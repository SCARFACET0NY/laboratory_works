package com.brainacad.module3.lesson3_8.lab1_2;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.List;

public class ThreadClient extends Thread {
    private Socket socket;
    private List<Student> students;

    public ThreadClient(Socket socket, List<Student> students) {
        this.socket = socket;
        this.students = students;
    }

    @Override
    public void run() {
        try {
            ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
            Student student = (Student) ois.readObject();

            for (Student st : students) {
                if (st.equals(student)) {
                    System.out.println(student);
                    socket.getOutputStream().write(("Response for student " + student.getId()).getBytes());
                    socket.getOutputStream().flush();
                    socket.getOutputStream().close();
                    socket.close();
                    return;
                }
            }
            System.out.println("Access forbidden");

        } catch (IOException | ClassNotFoundException ioe) {
            ioe.printStackTrace();
        }
    }
}
