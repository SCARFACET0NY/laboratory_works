package com.brainacad.module3.lesson3_8.lab1_2;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Student> students = new ArrayList<>();
        students.add(new Student("John", "Java", 1));
        students.add(new Student("Jack", "Java", 2));
        students.add(new Student("Jim", "Java", 3));
        students.add(new Student("Julio", "Java", 4));
        students.add(new Student("Juno", "Java", 5));

        MyServer server = new MyServer();
        server.initUsers(students);
        MyClient client1 = new MyClient(students.get(0));
        MyClient client2 = new MyClient(students.get(1));
        MyClient client3 = new MyClient(students.get(2));
        MyClient client4 = new MyClient(students.get(3));
        MyClient client5 = new MyClient(students.get(4));
        MyClient client6 = new MyClient(new Student("Jude", "Java", 8));

        server.start();
        client1.start();
        client2.start();
        client3.start();
        client4.start();
        client5.start();
        client6.start();
    }
}
