package com.brainacad.module3.lesson3_8.lab1_2;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;

public class MyServer extends Thread {
    private List<Student> users ;

    public void initUsers(List<Student> users) {
        this.users = users;
    }

    @Override
    public void run() {
        try (ServerSocket listener = new ServerSocket(2222)) {
            while (true) {
                Socket socket = listener.accept();
                ThreadClient thread = new ThreadClient(socket, users);
                thread.start();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
