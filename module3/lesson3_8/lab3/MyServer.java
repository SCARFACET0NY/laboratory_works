package com.brainacad.module3.lesson3_8.lab3;



import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class MyServer extends Thread {
    @Override
    public void run() {
        handlingQuery();
    }

    public void handlingQuery() {
        try (ServerSocket listener = new ServerSocket(3333)) {
            while (true) {
                Socket socket = listener.accept();
                ServerThread thread = new ServerThread(socket);
                thread.start();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public static void main(String[] args) {
        MyServer server = new MyServer();
        server.start();
    }
}
