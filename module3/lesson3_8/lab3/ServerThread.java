package com.brainacad.module3.lesson3_8.lab3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;


public class ServerThread extends Thread {
    private Socket socket;

    public ServerThread(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try (
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                PrintWriter out = new PrintWriter(socket.getOutputStream())
        ) {
            String request = in.readLine();
            out.write(request +"PONG \n");
            out.flush();
            socket.close();
        } catch (IOException  ioe) {
            ioe.printStackTrace();
        }
    }
}
