package com.brainacad.module3.lesson3_8.lab3;

import java.io.*;
import java.net.Socket;

public class MyClient extends Thread {
    private String name;

    public MyClient(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        exchangeData();
    }

    public void exchangeData() {
        for (int i = 0; i < 10; i++) {
            try (
                    Socket socket = new Socket("localhost", 3333);
                    BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    PrintWriter out = new PrintWriter(socket.getOutputStream())
            ) {
                out.write("PING-\n");
                out.flush();
                System.out.println(name + " " + in.readLine() + i);
                MyClient.sleep(1000);
                in.close();
                out.close();
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            } catch(IOException ioe){
                ioe.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        MyClient client1 = new MyClient("John");
        MyClient client2 = new MyClient("Steve");
        MyClient client3 = new MyClient("Larry");

        client1.start();
        client2.start();
        client3.start();
    }
}
