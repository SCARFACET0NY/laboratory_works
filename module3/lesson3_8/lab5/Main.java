package com.brainacad.module3.lesson3_8.lab5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class Main {
    public static void main(String[] args) {
        BufferedReader in = null;
        try {
            URL url = new URL("http://mainacad.com");
            URLConnection connection = url.openConnection();
            in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String line;
            while ((line = in.readLine()) != null) {
                System.out.println(line);
            }
        } catch (MalformedURLException mue) {
            mue.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            try {
                if (in != null) in.close();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }
}
