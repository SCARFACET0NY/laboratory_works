package com.brainacad.module3.lesson3_1.lab1;

import java.io.File;

public class MyFilesList {
    public static void main(String[] args) {
        File file = null;
        String[] paths;

        try {
            file = args.length == 0 ? new File(System.getProperty("user.dir")) : new File(args[0]);
            paths = file.list();

            for (String path : paths) {
                System.out.println(path);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
