package com.brainacad.module3.lesson3_1.lab5;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.print("Enter name: ");
        Scanner sc = new Scanner(System.in);
        String name = sc.nextLine();

        AccountingUser user = new AccountingUser();
        user.printFile();
        System.out.println();

        user.testUsers(name);
        user.printFile();
    }
}
