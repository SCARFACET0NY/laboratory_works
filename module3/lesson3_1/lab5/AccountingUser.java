package com.brainacad.module3.lesson3_1.lab5;

import java.io.IOException;
import java.io.RandomAccessFile;

public class AccountingUser {
    private RandomAccessFile users;

    public AccountingUser () {
        try {
            users = new RandomAccessFile("users.txt", "rw");
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void testUsers(String name) {
        try {
            String line;
            users.seek(0L);
            while ((line = users.readLine()) != null) {
                String[] words = line.split(":");
                if (words[0].equals(name)) {
                    words[1] = String.valueOf(Integer.parseInt(words[1]) + 1);
                    users.seek(users.getFilePointer() - line.length() - 2);
                    users.writeBytes(words[0] + ":" + words[1] + "\r\n");
                    return;
                }
            }
            users.writeBytes("\r\n" + name + ":1");
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return;
    }

    public void printFile() {
        try {
            String line;
            users.seek(0L);
            while ((line = users.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void finalize() {
        try {
            users.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
