package com.brainacad.module3.lesson3_1.lab2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

class PrintFile {
    public static void main(String[] args) {
        BufferedReader in = null;
        String read;
        try {
            in = new BufferedReader(new FileReader(args[0])); // args[0] -> text.txt
            while ((read = in.readLine()) != null) {
                System.out.println(read);
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            try {
                if (in != null) in.close();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }
}
