package com.brainacad.module3.lesson3_1.lab4;

import java.io.*;
import java.util.Scanner;

public class ReplacementInFile {
    public static void main(String[] args) {
        BufferedReader reader = null;
        BufferedWriter writer = null;
        String read = "";
        String line;

        System.out.print("Enter your file: ");
        Scanner sc = new Scanner(System.in);
        String file = sc.nextLine(); // Repl.java
        try {
            reader = new BufferedReader(new FileReader(file));
            while ((line = reader.readLine()) != null) {
                read += line.replace("public", "private") + "\n";
            }
            writer = new BufferedWriter(new FileWriter(file));
            writer.write(read);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            try {
                if (reader != null) reader.close();
                if (writer != null) writer.close();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }
}
