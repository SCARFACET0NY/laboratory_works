package com.brainacad.module3.lesson3_1.lab3;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class MyFilesCopy {
    public static void main(String[] args) {
        FileInputStream in = null;
        FileOutputStream out = null;
        int c;

        try {
            in = new FileInputStream(args[0]); // args[0] -> inputText.txt
            out = new FileOutputStream(args[1]); // args[0] -> outputText.txt
            while ((c = in.read()) != -1) {
                out.write(c);
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        finally {
            try {
                if (in != null) in.close();
                if (out != null) out.close();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }
}
