package com.brainacad.module2.lesson2_2.lab3;

public class Accountant {
    public static void main(String[] args) {
        Employee person = new Employee();
        person.calcSalary("Anton");
        person.calcSalary("Anton", 125.56);
        person.calcSalary("Anton", 450.0, 890.0, 950.0);
    }
}
