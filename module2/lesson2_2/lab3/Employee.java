package com.brainacad.module2.lesson2_2.lab3;

public class Employee {
    public void calcSalary(String name, double... salary) {
        double totalSalary = 0.0;
        for (double sal: salary) {
            totalSalary += sal;
        }

        System.out.println(name + "'s total salary: " + String.format("%.2f", totalSalary));
    }
}
