package com.brainacad.module2.lesson2_2.lab1_2;

public class Main {
    public static void main(String[] args) {

        Matrix matrix = new Matrix();
        final int SIZE = 3;
        int[][] matrix1 = matrix.initMatrix(SIZE);
        int[][] matrix2 = matrix.initMatrix(SIZE);
        matrix.printMatrix(matrix1);
        System.out.println();
        matrix.printMatrix(matrix2);
        System.out.println();

        matrix.printMatrix(matrix.multiplyMatrix(matrix1, matrix2));
        System.out.println();
        matrix.printMatrix(matrix.addMatrix(matrix1, matrix2));
    }


}
