package com.brainacad.module2.lesson2_2.lab1_2;

import java.util.Arrays;

public class Matrix {
    private boolean checkMatrix(int[][] firstMatrix, int[][] secondMatrix) {
        if (firstMatrix == null && secondMatrix == null && (firstMatrix.length != secondMatrix.length)) {
            return false;
        } else {
            int size = firstMatrix.length;
            for (int row = 0; row < size; row++) {
                if (firstMatrix[row].length != size || secondMatrix[row].length != size) {
                    return false;
                }
            }
        }

        return true;
    }

    public  void printMatrix(int[][] matrix) {
        for (int[] row : matrix) {
            System.out.println(Arrays.toString(row));
        }
    }

    public int[][] addMatrix(int[][] firstMatrix, int[][] secondMatrix) {

        if (!checkMatrix(firstMatrix, secondMatrix)) {
            return null;
        }

        int[][] result = new int[firstMatrix.length][firstMatrix.length];

        for (int i = 0; i < result.length; i++){
            for (int j = 0; j < result[i].length; j++) {
                result[i][j] = firstMatrix[i][j] + secondMatrix[i][j];
            }
        }

        return result;
    }

    public int[][] multiplyMatrix(int[][] firstMatrix, int[][] secondMatrix) {
        if (!checkMatrix(firstMatrix, secondMatrix)) {
            return null;
        }

        int[][] result = new int[firstMatrix.length][firstMatrix.length];

        for (int i = 0; i < result.length; i++){
            for (int j = 0; j < result[i].length; j++) {
                for (int k = 0; k < result.length; k++) {
                    result[i][j] += firstMatrix[i][k] * secondMatrix[k][j];
                }
            }
        }

        return result;
    }

    public int[][] initMatrix(int size) {
        int[][] matrix = new int[size][size];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = (int)(Math.random() * 10);
            }
        }

        return matrix;
    }
}
