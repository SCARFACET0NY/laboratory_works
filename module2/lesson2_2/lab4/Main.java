package com.brainacad.module2.lesson2_2.lab4;

public class Main {
    public static void main(String[] args) {
        Person anton = new Person();
        anton.setPerson("Anton");
        anton.setPerson("Anton", "Shevchenko");
        anton.setPerson("Anton", "Shevchenko", 30);
        anton.setPerson("Anton", "Shevchenko", 30, "Male");
        anton.setPerson("Anton", "Shevchenko", 30, "Male", 1234567);
        System.out.println(anton);
    }
}
