package com.brainacad.module2.lesson2_2.lab5;

public class A {
    private void calcSquare(int a, int b) {
        System.out.println("Rectangle - " + a * b);
    }

    private void calcSquare(int a) {
        System.out.println("Square - " + a * a);
    }

    private void calcSquare(double r) {
        System.out.println("Circle - " + Math.PI * r * r);
    }

//    private void finalTest (final int a) {
//        a *= a;
//    }

    public static void main(String[] args) {
        A rectangle = new A();
        A square = new A();
        A circle = new A();

        rectangle.calcSquare(5, 4);
        square.calcSquare(5);
        circle.calcSquare(2.0);
    }
}
