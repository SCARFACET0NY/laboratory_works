package com.brainacad.module2.lesson2_14.lab4;

import java.util.Arrays;
import java.util.Random;

public class MyMixer<T> {
    private T[] t;

    public MyMixer(T[] t) {
        this.t = t;
    }

    public T[] shuffle() {
        Random rand = new Random();

        for (int i = 0; i < t.length; i++) {
            int randomIndex = rand.nextInt(t.length);
            T temp = t[i];
            t[i] = t[randomIndex];
            t[randomIndex] = temp;
        }

        return t;
    }
}

class Main {
    public static void main(String[] args) {
        Integer[] integers = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        String [] strings = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};

        MyMixer<Integer> mixer1 = new MyMixer<>(integers);
        MyMixer<String> mixer2 = new MyMixer<>(strings);

        System.out.println(Arrays.toString(mixer1.shuffle()));
        System.out.println(Arrays.toString(mixer2.shuffle()));
    }
}
