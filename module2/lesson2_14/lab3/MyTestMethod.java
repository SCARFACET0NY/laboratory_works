package com.brainacad.module2.lesson2_14.lab3;

import java.math.BigDecimal;
import java.util.Arrays;

public class MyTestMethod {
    public static <T extends Number & Comparable<T>> BigDecimal calcSum(T[] arr, T maxValue) {
        BigDecimal sum = BigDecimal.ZERO;
        for (T elem : arr) {
            if (elem.compareTo(maxValue) > 0) {
                sum = sum.add(new BigDecimal(elem.toString()));
            }
        }
        return sum;
    }
}

class Main {
    public static void main(String[] args) {
        Integer[] integers = {15, 10, 7, 16, 21, 2, 6, 32, 1, 18, 7, 24};
        Double[] doubles = {15.1, 10.9, 7.4, 16.5, 21.2, 2.7, 6.8, 32.6, 1.3, 18.1, 7.2, 24.3};
        Integer maxElem1 = 12;
        Double maxElem2 = 8.8;

        System.out.println(Arrays.toString(integers));
        System.out.println("Sum of elements that are greater than " + maxElem1 + " : " + MyTestMethod.calcSum(integers, maxElem1));
        System.out.println(Arrays.toString(doubles));
        System.out.println("Sum of elements that are greater than " + maxElem2 + " : " + MyTestMethod.calcSum(doubles, maxElem2));
    }
}
