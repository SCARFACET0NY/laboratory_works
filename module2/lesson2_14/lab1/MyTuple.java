package com.brainacad.module2.lesson2_14.lab1;

public class MyTuple<A, B, C> {
    private A a;
    private B b;
    private C c;

    public MyTuple(A a, B b, C c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public A getA() {
        return a;
    }

    public B getB() {
        return b;
    }

    public C getC() {
        return c;
    }
}

class Main {
    public static void main(String[] args) {
        MyTuple<String, Integer, Long> myTuple1 = new MyTuple<>("Tuple 1", 1, 1L);
        MyTuple<Double, String, String> myTuple2 = new MyTuple<>(1.0, "Tuple 2", "Tuple 2.0");
    }
}