package com.brainacad.module2.lesson2_14.lab2;

import java.util.Arrays;

public class MyTestMethod {
    public static <T extends Comparable<T>> int calcNum(T[] arr, T maxElem) {
        int count = 0;
        for (T elem : arr) {
            if (elem.compareTo(maxElem) > 0) {
                count++;
            }
        }
        return count;
    }
}

class Main {
    public static void main(String[] args) {
        Integer[] integers = {15, 10, 7, 16, 21, 2, 6, 32, 1, 18, 7, 24};
        Double[] doubles = {15.1, 10.9, 7.4, 16.5, 21.2, 2.7, 6.8, 32.6, 1.3, 18.1, 7.2, 24.3};
        Integer maxElem1 = 12;
        Double maxElem2 = 8.8;

        System.out.println(Arrays.toString(integers));
        System.out.println("Number of elements that are greater than " + maxElem1 + " : " + MyTestMethod.calcNum(integers, maxElem1));
        System.out.println(Arrays.toString(doubles));
        System.out.println("Number of elements that are greater than " + maxElem2 + " : " + MyTestMethod.calcNum(doubles, maxElem2));
    }
}
