package com.brainacad.module2.lesson2_13.lab1_2_3;

import java.util.Scanner;

public class Main {
    enum MyDayOfWeek {
        SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY;

        public MyDayOfWeek nextDay() {
            int next = (ordinal() == MyDayOfWeek.values().length - 1) ? 0 : ordinal() + 1;
            return values()[next];
        }
    }

    public static void main(String[] args) {
        //2.13.1
        for (MyDayOfWeek day: MyDayOfWeek.values()) {
            System.out.println(day);
        }
        System.out.println();

        //2.13.2
        for (MyDayOfWeek day: MyDayOfWeek.values()) {
            switch (day) {
                case MONDAY: case WEDNESDAY: case FRIDAY:
                    System.out.println("My Java day: " + day);
                    break;
                case TUESDAY: case THURSDAY: case SATURDAY: case SUNDAY:
                    break;
                default:
                    System.out.println("Unknown day");
            }
        }
        System.out.println();

        //2.13.3
        System.out.print("Please enter day of the week: ");
        Scanner sc = new Scanner(System.in);
        String inputDay = sc.nextLine().toUpperCase();
        sc.close();

        MyDayOfWeek dayOfWeek = MyDayOfWeek.valueOf(inputDay);
        System.out.println(dayOfWeek);
    }
}
