package com.brainacad.module2.lesson2_13.lab5;

import java.util.Arrays;
import java.util.Scanner;

public class TrainSchedule {
    enum DayOfWeek {
        SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY
    }

    private Train[] trains;

    public TrainSchedule(Train[] trains) {
        this.trains = trains;
    }

    public void addTrain() {
        System.out.print("Please enter your train: ");
        Scanner sc = new Scanner(System.in);

        while (sc.hasNext()) {
            String line = sc.nextLine();
            if (!line.equalsIgnoreCase("q")) {
                String[] trainInfo = line.split(" ");
                Train train = new Train(Integer.parseInt(trainInfo[0]));
                train.setStationDispatch(trainInfo[1]);
                train.setTimeDispatch(trainInfo[2]);
                train.setStationArrival(trainInfo[3]);
                train.setTimeArrival(trainInfo[4]);
                String[] days = Arrays.copyOfRange(trainInfo, 5, trainInfo.length);
                DayOfWeek[] daysOfWeek = new DayOfWeek[days.length];
                for (int i = 0; i < days.length; i++) {
                    daysOfWeek[i] = DayOfWeek.valueOf(days[i].toUpperCase());
                }
                train.setDays(daysOfWeek);

                for (int i = 0; i < trains.length; i++) {
                    if (trains[i] == null) {
                        trains[i] = train;
                        break;
                    }
                }
            } else {
                break;
            }
        }
    }

    public void printTrains() {
        for (Train train: trains) {
            if (train != null) {
                System.out.println(train);
            }
        }
    }

    public void searchTrain(String stationDispatch, String stationArrival, DayOfWeek day) {
        boolean noTrains = true;
        for (int i = 0; i < trains.length; i++) {
            if (trains[i] != null && trains[i].getStationDispatch().equalsIgnoreCase(stationDispatch) &&
                trains[i].getStationArrival().equalsIgnoreCase(stationArrival) &&
                Arrays.asList(trains[i].getDays()).contains(day)) {

                System.out.println(trains[i]);
                noTrains = false;
            }
        }
        if(noTrains) {
            System.out.println("No trains found");
        }
    }

    class Train {
        private int number;
        private String stationDispatch;
        private String stationArrival;
        private String timeDispatch;
        private String timeArrival;
        private DayOfWeek[] days;

        public Train(int number) {
            this.number = number;
        }

        public String getStationDispatch() {
            return stationDispatch;
        }

        public void setStationDispatch(String stationDispatch) {
            this.stationDispatch = stationDispatch;
        }

        public String getStationArrival() {
            return stationArrival;
        }

        public void setStationArrival(String stationArrival) {
            this.stationArrival = stationArrival;
        }

        public String getTimeDispatch() {
            return timeDispatch;
        }

        public void setTimeDispatch(String timeDispatch) {
            this.timeDispatch = timeDispatch;
        }

        public String getTimeArrival() {
            return timeArrival;
        }

        public void setTimeArrival(String timeArrival) {
            this.timeArrival = timeArrival;
        }

        public DayOfWeek[] getDays() {
            return days;
        }

        public void setDays(DayOfWeek[] days) {
            this.days = days;
        }

        @Override
        public String toString() {
            return "Train: " + "number = " + number +
                    ", stationDispatch = " + stationDispatch  + ", timeDispatch = " + timeDispatch +
                    ", stationArrival = " + stationArrival + ", timeArrival = " + timeArrival +
                    ", days = " + Arrays.toString(days);
        }
    }
}

class Main {
    public static void main(String[] args) {
        TrainSchedule trainSchedule = new TrainSchedule(new TrainSchedule.Train[14]);
        trainSchedule.addTrain();
        trainSchedule.printTrains();
        System.out.println();
        trainSchedule.searchTrain("Kyiv", "Lviv", TrainSchedule.DayOfWeek.MONDAY);
    }
}
//1 Kyiv 10:00 Lviv 15:00 monday tuesday
//2 Dnipro 14:00 Odessa 18:00 wednesday thursday
//3 Madrid 17:00 Barcelona 20:00 friday saturday sunday
//4 Paris 20:00 Rome 06:00 monday wednesday friday sunday