package com.brainacad.module2.lesson2_13.lab4;

class Card {
    private Main.Suit cardSuit;
    private Main.Rank cardRank;

    public Card(Main.Suit cardSuit, Main.Rank cardRank) {
        this.cardSuit = cardSuit;
        this.cardRank = cardRank;
    }

    @Override
    public String toString() {
        return "The Card: "+ cardRank + "_" + cardSuit;
    }
}

public class Main {
    enum Suit {
        SPADE, DIAMOND, CLUB, HEART
    }

    enum Rank {
        ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING
    }

    public static void main(String[] args) {
        Card[] cardDeck = new Card[Suit.values().length * Rank.values().length];
        int count = 0;
        for (Suit suit : Suit.values()) {
            for (Rank rank : Rank.values()) {
                cardDeck[count] = new Card(suit, rank);
                System.out.println(cardDeck[count++]);
            }
        }


    }
}
