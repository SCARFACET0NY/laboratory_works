package com.brainacad.module2.lesson2_3.lab1_2_3_4_5;

public class MyWindow {
    private double width;
    private double height;
    private int numberOfGlass;
    private String color;
    private boolean isOpen;

    public MyWindow(double width, double height, int numberOfGlass, String color, boolean isOpen) {
        this.width = width;
        this.height = height;
        this.numberOfGlass = numberOfGlass;
        this.color = color;
        this.isOpen = isOpen;
    }

    public MyWindow(double width, double height) {
        this();
        this.width = width;
        this.height = height;
    }

    public MyWindow(double width, double height, int numberOfGlass) {
        this(width, height, numberOfGlass, "green", true);
    }

    public MyWindow() {
        width = 2.0;
        height = 3.0;
        numberOfGlass = 2;
        color = "green";
        isOpen = false;
    }



    public void printFields() {
        System.out.println("Width -> " + width);
        System.out.println("Height -> " + height);
        System.out.println("Number of glasses -> " + numberOfGlass);
        System.out.println("Color -> " + color);
        System.out.println("Is open -> " + isOpen);
    }

    public static void main(String[] args) {
        MyWindow defaultWindow = new MyWindow();
        MyWindow window = new MyWindow(10.0, 15.0, 4, "red", true);
        MyWindow window2 = new MyWindow(8.0, 12.0);
        MyWindow window3 = new MyWindow(9.0, 13.0, 1);

        MyWindow[] myWindows = {defaultWindow, window, window2, window3};
        for (MyWindow wind: myWindows) {
            wind.printFields();
            System.out.println();
        }
    }
}
