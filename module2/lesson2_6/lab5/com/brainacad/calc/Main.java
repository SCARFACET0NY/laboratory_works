package com.brainacad.module2.lesson2_6.lab5.com.brainacad.calc;

import com.brainacad.module2.lesson2_6.lab5.com.brainacad.shapes.Triangle;

public class Main {
    public static void main(String[] args) {
        Triangle trian = new Triangle(3, 4, 5);
        System.out.println(trian.getArea());
    }
}
