package com.brainacad.module2.Lesson2_18.lab1;

import java.math.BigInteger;
import java.util.Random;

public class Main {
    public BigInteger factorial(int n) {
        BigInteger fact = new BigInteger(String.valueOf(n));
        if (n == 1) {
            return BigInteger.ONE;
        }
        return fact.multiply(factorial(n - 1));
    }

    public static void main(String[] args) {
        Main main = new Main();
        Random random = new Random();
        int randNum = 10 + random.nextInt(40);

        System.out.println(randNum);
        System.out.println(main.factorial(randNum));
    }
}