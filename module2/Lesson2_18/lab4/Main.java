package com.brainacad.module2.Lesson2_18.lab4;

import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String[] programs = {"calc.exe", "cmd.exe", "control.exe", "mspaint.exe", "notepad.exe", "resmon.exe"};
        Random random = new Random();

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the program you want to run: ");
        String program = sc.nextLine();
        try {
//            Process process = Runtime.getRuntime().exec(programs[random.nextInt(programs.length)]);
            Process inputProcess = Runtime.getRuntime().exec(program);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
