package com.brainacad.module2.Lesson2_18.lab3;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.Date;
import java.util.Locale;

public class Main {
    public static void main(String[] args) {
        Locale loc = new Locale("Ukrainian", "UA");
        int number = 1111111;
        double partial = 0.05;

        NumberFormat numFmt = NumberFormat.getInstance(loc);
        NumberFormat curFmt = NumberFormat.getCurrencyInstance(loc);
        Currency cur = Currency.getInstance(loc);
        Date date = new Date();

        System.out.printf("Current Locale: %s (%s)%n", loc.getDisplayLanguage(), loc.getDisplayCountry());
        System.out.println("Integer: " + numFmt.format(number));
        System.out.println("Double: " + numFmt.format(partial));
        System.out.println("Currency: " + cur.getDisplayName() + ": " + curFmt.format(number));
        System.out.println("Date: " + date);
        System.out.println();

        NumberFormat numItl = NumberFormat.getInstance(Locale.ITALY);
        NumberFormat numChn = NumberFormat.getInstance(Locale.CHINA);

        System.out.println(numItl.format(number));
        System.out.println(numChn.format(number));
    }
}
