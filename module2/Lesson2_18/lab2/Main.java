package com.brainacad.module2.Lesson2_18.lab2;

import java.math.BigDecimal;

public class Main {
    public final BigDecimal dollar = new BigDecimal("1.0");

    public void purchaseFor1$(String nail) {
        BigDecimal price = new BigDecimal(nail);
        BigDecimal dollar = this.dollar;

        int count = 0;
        while (dollar.compareTo(price) >= 0) {
            dollar = dollar.subtract(price);
            count++;
        }

        System.out.println(count + " items bought");
        System.out.println("Money left over: $" + dollar);
    }

    public void purchaseFor1$(String[] nails) {
        BigDecimal dollar = this.dollar;

        int count = 0;
        for (int i = 0; i < nails.length; i++) {
            BigDecimal price = new BigDecimal(nails[i]);
            if (dollar.compareTo(price) >= 0) {
                dollar = dollar.subtract(price);
                count++;
            }
        }

        System.out.println(count + " items bought");
        System.out.println("Money left over: $" + dollar);
    }

    public static void main(String[] args) {
        String nail1 = "0.1", nail2 = "0.2", nail3 = "0.3", nail4 = "0.4";
        String[] nails = {nail1, nail2, nail3, nail4};

        Main main = new Main();
        main.purchaseFor1$(nail1);
        main.purchaseFor1$(nail2);
        main.purchaseFor1$(nail3);
        main.purchaseFor1$(nail4);
        main.purchaseFor1$(nails);
    }
}
