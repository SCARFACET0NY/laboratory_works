package com.brainacad.module2.lesson2_15.lab2;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        List<String> list = new LinkedList<>();
        final int length = 10;

        Random random = new Random();
        for (int i = 0; i < length; i++) {
            list.add(random.nextInt(i + 1) ,"number_" + i);
        }

        for (String item: list) {
            System.out.println(item);
        }
    }
}
