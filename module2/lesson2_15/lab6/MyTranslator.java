package com.brainacad.module2.lesson2_15.lab6;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class MyTranslator {
    private Map<String, String> dictionary = new HashMap<>();

    public void addNewWord(String en, String ru) {
        dictionary.put(en, ru);
    }

    public void translate(String line) {
        String[] words = line.split(" ");
        for (String word: words) {
            System.out.print(dictionary.get(word) + " ");
        }
    }
}

class Main {
    public static void main(String[] args) {
        MyTranslator myTranslator = new MyTranslator();
        myTranslator.addNewWord("cat", "кот");
        myTranslator.addNewWord("caught", "поймал");
        myTranslator.addNewWord("mouse", "мышь");

        System.out.print("Please enter your word: ");
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine();
        myTranslator.translate(line);
        sc.close();
    }
}
