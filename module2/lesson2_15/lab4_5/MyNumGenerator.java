package com.brainacad.module2.lesson2_15.lab4_5;

import java.util.*;

class MyNumGenerator {
    private int numOfElm;
    private int maxNumb;

    public MyNumGenerator(int numOfElm, int maxNumb) {
        this.numOfElm = numOfElm;
        this.maxNumb = maxNumb;
    }

    public List generate() {
        List<Integer> list = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < numOfElm; i++) {
            list.add(random.nextInt(maxNumb));
        }
        return list;
    }

    public Set generateDistinct() {
        if (numOfElm > maxNumb) {
            System.out.println("Impossible to create set, because number of elements > max number");
            return null;
        }

        Set<Integer> set = new HashSet<>();
        Random random = new Random();

        while (set.size() < numOfElm) {
            set.add(random.nextInt(maxNumb));
        }

        return set;
    }
}

class Main {
    public static void main(String[] args) {
        //2.15.4
        MyNumGenerator myNumGenerator = new MyNumGenerator(5, 64);
        System.out.println(myNumGenerator.generate());

        //2.15.5
        System.out.println(myNumGenerator.generateDistinct());
    }
}
