package com.brainacad.module2.lesson2_15.lab3;

import java.util.*;

public class Main {
    public static void printElements(Collection c) {
        Iterator itr = c.iterator();
        while (itr.hasNext()) {
            System.out.println(itr.next());
        }
    }

    public static void main(String[] args) {
        List<String> aList = new ArrayList<>();
        List<String> lList = new LinkedList<>();
        final int length = 10;

        for (int i = 0; i < length; i++) {
            aList.add("number_" + i);
            lList.add("number_" + i);
        }

        Iterator<String> itr1 = aList.iterator();
        while (itr1.hasNext()) {
            System.out.println(itr1.next());
        }
        System.out.println();

        Iterator<String> itr2 = lList.iterator();
        while (itr2.hasNext()) {
            System.out.println(itr2.next());
        }
        System.out.println();

        ListIterator<String> itr3 = aList.listIterator();
        while (itr3.hasNext()) {
            lList.add(itr3.next());
        }

        while (itr3.hasPrevious()) {
            lList.add(itr3.previous());
        }

        Main.printElements(lList);
    }
}
