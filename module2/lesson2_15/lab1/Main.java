package com.brainacad.module2.lesson2_15.lab1;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        final int length = 10;

        for (int i = 0; i < length; i++) {
            list.add("number_" + i);
        }

        for (String item: list) {
            System.out.println(item);
        }
    }
}
