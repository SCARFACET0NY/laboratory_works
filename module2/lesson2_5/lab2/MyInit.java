package com.brainacad.module2.lesson2_5.lab2;

import java.util.Arrays;
import java.util.Random;

public class MyInit {
    public int[] arr;

    {
        arr = new int[10];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = new Random().nextInt(100);
        }
    }

    public void printArray() {
        System.out.println(Arrays.toString(arr));
    }
}

class Main {
    public static void main(String[] args) {
        MyInit arr1 = new MyInit();
        MyInit arr2 = new MyInit();

        arr1.printArray();
        arr2.printArray();
    }
}