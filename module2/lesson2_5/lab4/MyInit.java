package com.brainacad.module2.lesson2_5.lab4;

import java.util.Arrays;
import java.util.Random;

public class MyInit {
    public static int[] arr;

    static {
        arr = new int[10];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = new Random().nextInt(100);
        }
    }

    public static void printArray() {
        System.out.println(Arrays.toString(arr));
    }
}

class Main {
    public static void main(String[] args) {
        MyInit.printArray();
        MyInit arr = new MyInit();
        arr.printArray();
    }
}
