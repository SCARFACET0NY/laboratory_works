package com.brainacad.module2.lesson2_5.lab1;

public class MyInitTest {
    {
        System.out.println("non-static initialization block 1");
    }

    static {
        System.out.println("static initialization block 1");
    }

    {
        System.out.println("non-static initialization block 2");
    }

    static {
        System.out.println("static initialization block 2");
    }

    public MyInitTest() {
        this(5);
        System.out.println("constructor 2");
    }

    public MyInitTest(int a) {
        System.out.println("constructor 1");
    }
}

class Main {
    public static void main(String[] args) {
        MyInitTest test = new MyInitTest();
    }
}
