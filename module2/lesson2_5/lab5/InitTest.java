package com.brainacad.module2.lesson2_5.lab5;

import java.util.Random;

public class InitTest {
    private int id;
    private static int nextId;

    static {
        nextId = new Random().nextInt(1000);
    }

    {
        id = ++nextId;
    }

    public int getId() {
        return id;
    }
}

class main {
    public static void main(String[] args) {
        System.out.println(new InitTest().getId());
        System.out.println(new InitTest().getId());
        System.out.println(new InitTest().getId());
        System.out.println(new InitTest().getId());
        System.out.println(new InitTest().getId());
    }
}