package com.brainacad.module2.lesson2_9.lab3;

import java.util.Arrays;

public class Main {
    private static char[] uniqueChars(String s) {
        char[] chars = s.toLowerCase().toCharArray();
        char[] uniqueChars = new char[(int)s.toLowerCase().chars().distinct().count()];

        int count = 0;
        outer:
        for (int i = 0; i < chars.length; i++) {
            for (int j = i + 1; j < chars.length; j++) {
                if (chars[i] == chars[j]) {
                    count++;
                    continue outer;
                }
            }
            uniqueChars[i - count] = chars[i];
        }
        Arrays.sort(uniqueChars);

        return uniqueChars;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(uniqueChars("Using methods of class String")));
    }
}
