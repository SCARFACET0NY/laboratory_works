package com.brainacad.module2.lesson2_9.lab4;

import java.util.StringTokenizer;

public class Main {
    public static void main(String[] args) {
        String myStr = "This is String, split by StringTokenizer. Created by Student: Shevchenko Anton";

        StringTokenizer s1 = new StringTokenizer(myStr, " |,|.");

        while (s1.hasMoreElements()) {
            System.out.println(s1.nextElement());
        }
        System.out.println();
    }
}
