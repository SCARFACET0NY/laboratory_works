package com.brainacad.module2.lesson2_9.lab2;

public class Main {
    public static void main(String[] args) {
        String myStr1 = "Cartoon";
        String myStr2 = "Tomcat";

        myStr1 = myStr1.toLowerCase();
        myStr2 = myStr2.toLowerCase();

        StringBuilder newStr = new StringBuilder("");
        outer:
        for (int i = 0; i < myStr1.length(); i++) {
            for (int j = 0; j < myStr2.length(); j++) {
                if(j < newStr.length() && newStr.charAt(j) == myStr1.charAt(i)) {
                    continue outer;
                }
                if (myStr1.charAt(i) == myStr2.charAt(j)) {
                    continue outer;
                }
            }
            newStr.append(myStr1.charAt(i));
        }

        System.out.println(newStr);
    }
}
