package com.brainacad.module2.lesson2_9.lab5;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    private static boolean checkPersonWithRegExp(String s) {
        Pattern pat = Pattern.compile("^[A-Z][a-z]+");
        Matcher mat = pat.matcher(s);

        return mat.matches();
    }

    public static void main(String[] args) {
        String[] arr1 = {"VOVA", "Ivan", "R2d2", "ZX", "Anna", "12345", "ToAd", "TomCat", " "};

        for (String str: arr1) {
            if (checkPersonWithRegExp(str)) {
                System.out.println(str);
            }
        }
    }
}
