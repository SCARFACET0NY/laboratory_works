package com.brainacad.module2.lesson2_9.lab1;

public class Main {
    private static StringBuilder reverseString(String str) {
         return new StringBuilder(str).reverse();
    }

    public static void main(String[] args) {
        String myStr = "abracadabra";

        System.out.println(myStr.indexOf("ra"));
        System.out.println(myStr.lastIndexOf("ra"));
        System.out.println(myStr.substring(3, 7));
        System.out.println(reverseString(myStr));
    }
}
