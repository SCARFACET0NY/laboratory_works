package com.brainacad.module2.lesson2_11.lab3;

public class InvalidAgeException extends RuntimeException {
    private String message;

    public InvalidAgeException(String msg) {
        message = msg;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
