package com.brainacad.module2.lesson2_11.lab3;

public class Main {
    public static void main(String[] args) {
        try {
            Person johnny = new Person();
            johnny.setAge(130);
            System.out.println(johnny.getAge());
        } catch (InvalidAgeException iae) {
            System.out.println(iae.getMessage());
        }
    }
}
