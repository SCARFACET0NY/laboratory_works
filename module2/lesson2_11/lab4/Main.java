package com.brainacad.module2.lesson2_11.lab4;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try {
            Shape triangle = Shape.parseShape("Triangle: Red: 8, 5, 4");
            Shape rectangle = Shape.parseShape("Rectangle: Green: 5, 4");
            Shape circle = Shape.parseShape("Circle: Blue: 4");
            triangle.draw();
            rectangle.draw();
            circle.draw();
            System.out.println();

            System.out.print("Please enter shape and parameters: ");
            Scanner sc = new Scanner(System.in);
            Shape shape = Shape.parseShape(sc.nextLine());
            shape.draw();;
        } catch (InvalidShapeStringException ise) {
            System.out.println(ise.getMessage());
        }
    }
}
