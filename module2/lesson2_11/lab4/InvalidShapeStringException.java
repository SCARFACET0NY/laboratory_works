package com.brainacad.module2.lesson2_11.lab4;

public class InvalidShapeStringException extends Exception {
    private String message;

    public InvalidShapeStringException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
