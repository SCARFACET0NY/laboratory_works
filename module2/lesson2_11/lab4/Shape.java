package com.brainacad.module2.lesson2_11.lab4;

public abstract class Shape implements Drawable, Comparable {
    private String shapeColor;

    public Shape(String shapeColor) {
        this.shapeColor = shapeColor;
    }

    public void draw() {
        System.out.print("This is " + getClass().getSimpleName() + ", color is " + shapeColor);
    }

    public abstract double calcArea();

    public String getShapeColor() {
        return shapeColor;
    }

    @Override
    public int compareTo(Object o) {
        Shape comp = (Shape)o;

        if (this.calcArea() > comp.calcArea()) {
            return 1;
        } else if (this.calcArea() < comp.calcArea()) {
            return -1;
        } else {
            return 0;
        }
    }

    public static Shape parseShape(String str) throws InvalidShapeStringException {
        String arr[] = str.split(" |,|:|;");
        String first = arr[0].toLowerCase();

        if(arr.length <= 1 || (!first.equals("triangle") && !first.equals("rectangle") && !first.equals("circle"))) {
            throw new InvalidShapeStringException("Parse error: Invalid Shape");
        }

        switch (first) {
            case "triangle":
                return Triangle.parseTriangle(str);
            case "rectangle":
                return Rectangle.parseRectangle(str);
            case "circle":
                return Circle.parseCircle(str);
            default:
                return null;
        }
    }
}
