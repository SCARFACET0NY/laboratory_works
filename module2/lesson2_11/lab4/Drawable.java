package com.brainacad.module2.lesson2_11.lab4;

public interface Drawable {
    void draw();
}
