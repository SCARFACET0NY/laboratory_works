package com.brainacad.module2.lesson2_11.lab4;

public class Rectangle extends Shape {
    private double width;
    private double height;

    public Rectangle(String shapeColor, double width, double height) {
        super(shapeColor);
        this.width = width;
        this.height = height;
    }

    @Override
    public void draw() {
        super.draw();
        System.out.println(", width = " + width + ", height = " + height);
        System.out.println("Shape area is: " + String.format("%.2f", calcArea()));
    }

    @Override
    public double calcArea() {
        return width * height;
    }

    public static Rectangle parseRectangle(String str) {
        str = str.replaceAll("[,:;]", "");
        String arr[] = str.split(" ");

        double[] arguments = new double[3];
        int count = 0;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i].matches("[0-9]*.[0-9]*")) {
                arguments[count++] = Double.parseDouble(arr[i]);
            }
        }

        if (arr[0].toLowerCase().equals("rectangle")) {
            return new Rectangle(arr[1], arguments[0], arguments[1]);
        } else {
            return null;
        }
    }
}
