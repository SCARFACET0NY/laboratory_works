package com.brainacad.module2.lesson2_11.lab5;

public class Main {
    private static Long compute(Short s, Integer i) {
        return new Long(s + i);
    }

    public static void main(String[] args) {
        Short s = 5;
        Integer i = 10;

        Long sum = compute(s, i);

        assert sum == 5: "Value must equal 5";

        System.out.println(sum);
    }
}
