package com.brainacad.module2.lesson2_11.lab2;

public class Main {
    public static void main(String[] args) {
        try {
//            MyTest mytest = new MyTest();
//            mytest.test();
            MyTest mytest1 = null;
            mytest1.test();
        } catch (MyException me) {
            System.out.println(me.getErrorString());
        } catch (NullPointerException npe) {
            System.out.println(npe.getMessage());
        }
    }
}
