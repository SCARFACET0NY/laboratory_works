package com.brainacad.module2.lesson2_11.lab2;

public class MyException extends Exception {
    private String errorString;

    public MyException(String str) {
        errorString = str;
    }

    public String getErrorString() {
        return errorString;
    }
}
