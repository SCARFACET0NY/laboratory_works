package com.brainacad.module2.lesson2_10.lab2;

public class Main {
    private static Long compute(Short s, Integer i) {
        return new Long(s + i);
    }

    public static void main(String[] args) {
        Short s = 5;
        Integer i = 10;

        System.out.println(compute(s, i));
        System.out.println(compute(s, i).getClass().getSimpleName());
    }
}
