package com.brainacad.module2.lesson2_10.lab1;

public class Main {
    public static void main(String[] args) {
        Integer x1 = 11;
        Integer x2 = new Integer(11);
        Integer x3 = Integer.valueOf(11);
        Integer x4 = Integer.parseInt("11");

        System.out.println(x1 == x2);
        System.out.println(x1.equals(x2));
        System.out.println(x1 == x3);
        System.out.println(x1.equals(x3));
        System.out.println(x1 == x4);
        System.out.println(x1.equals(x4));
        System.out.println();

        Integer x5 = 180;
        Integer x6 = new Integer(180);
        Integer x7 = Integer.valueOf(180);
        Integer x8 = Integer.parseInt("180");

        System.out.println(x5 == x6);
        System.out.println(x5.equals(x6));
        System.out.println(x5 == x7);
        System.out.println(x5.equals(x7));
        System.out.println(x5 == x8);
        System.out.println(x5.equals(x8));
    }
}
