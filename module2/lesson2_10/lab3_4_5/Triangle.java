package com.brainacad.module2.lesson2_10.lab3_4_5;

import static java.lang.Math.sqrt;

public class Triangle extends Shape {
    private double a;
    private double b;
    private double c;

    public Triangle(String shapeColor, double a, double b, double c) {
        super(shapeColor);
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public void draw() {
        super.draw();
        System.out.println(", a = " + a + ", b = " + b + ", c = " + c);
        System.out.println("Shape area is: " + String.format("%.2f", calcArea()));
    }

    @Override
    public double calcArea() {
        double s = (a + b + c) / 2;
        return sqrt(s * (s - a) * (s - b) * (s - c));
    }

    public static Triangle parseTriangle(String str) {
        String arr[] = str.split(" |,|:|;");

        double[] arguments = new double[3];
        int count = 0;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i].matches("[0-9]*.[0-9]*")) {
                arguments[count++] = Double.parseDouble(arr[i]);
            }
        }

        if (arr[0].toLowerCase().equals("triangle")) {
            return new Triangle(arr[1], arguments[0], arguments[1], arguments[2]);
        } else {
            return null;
        }
    }
}


