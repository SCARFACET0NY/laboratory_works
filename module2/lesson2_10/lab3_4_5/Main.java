package com.brainacad.module2.lesson2_10.lab3_4_5;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //2.10.3
        Shape parsedRectangle = Shape.parseShape("Rectangle: Red: 5, 6");
        Shape parsedTriangle = Shape.parseShape("Triangle: Green: 9, 7, 12");
        Shape parsedCircle = Shape.parseShape("Circle: Black: 10");

        parsedRectangle.draw();
        parsedTriangle.draw();
        parsedCircle.draw();
        System.out.println();

        //2.10.4
        Rectangle parsRect = Rectangle.parseRectangle("Rectangle: Red: 5, 6");
        Triangle parsTrian = Triangle.parseTriangle("Triangle: Green: 2, 3, 4");
        Circle parsCirc = Circle.parseCircle("Circle: Blue: 2.5");

        parsRect.draw();
        parsTrian.draw();
        parsCirc.draw();

        //2.10.5
        System.out.print("Please enter an integer: ");
        Scanner sc = new Scanner(System.in);
        Integer inputInt = Integer.parseInt(sc.nextLine());

        Shape[] parsedShapes = new Shape[inputInt];
        int count = 0;
        while (sc.hasNext()) {
            String line = sc.nextLine();
            if (!line.equalsIgnoreCase("q")) {
                parsedShapes[count] = Shape.parseShape(line);
                count++;
            } else {
                break;
            }
        }
        sc.close();
        for (Shape sh: parsedShapes) {
            sh.draw();
        }
    }
}
