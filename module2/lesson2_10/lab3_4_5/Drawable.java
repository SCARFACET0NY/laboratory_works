package com.brainacad.module2.lesson2_10.lab3_4_5;

public interface Drawable {
    void draw();
}
