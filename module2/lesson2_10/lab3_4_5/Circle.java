package com.brainacad.module2.lesson2_10.lab3_4_5;

public class Circle extends Shape {
    private double radius;

    public Circle(String shapeColor ,double radius) {
        super(shapeColor);
        this.radius = radius;
    }

    @Override
    public void draw() {
        super.draw();
        System.out.println(", radius = " + radius);
        System.out.println("Shape area is: " + String.format("%.2f", calcArea()));
    }

    @Override
    public double calcArea() {
        return Math.PI * radius * radius;
    }

    public static Circle parseCircle(String str) {
        str = str.replaceAll("[,:;]", "");
        String arr[] = str.split(" ");

        double[] arguments = new double[3];
        int count = 0;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i].matches("[0-9]*.[0-9]*")) {
                arguments[count++] = Double.parseDouble(arr[i]);
            }
        }

        if (arr[0].toLowerCase().equals("circle")) {
            return new Circle(arr[1], arguments[0]);
        } else {
            return null;
        }
    }
}
