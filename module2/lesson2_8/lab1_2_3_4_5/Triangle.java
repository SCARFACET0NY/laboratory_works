package com.brainacad.module2.lesson2_8.lab1_2_3_4_5;

import static java.lang.Math.sqrt;

public class Triangle extends Shape {
    private double a;
    private double b;
    private double c;

    public Triangle(String shapeColor, double a, double b, double c) {
        super(shapeColor);
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public void draw() {
        super.draw();
        System.out.println(", a = " + a + ", b = " + b + ", c = " + c);
        System.out.println("Shape area is: " + String.format("%.2f", calcArea()));
    }

    @Override
    public double calcArea() {
        double s = (a + b + c) / 2;
        return sqrt(s * (s - a) * (s - b) * (s - c));
    }
}


