package com.brainacad.module2.lesson2_8.lab1_2_3_4_5;

public abstract class Shape implements Drawable, Comparable {
    private String shapeColor;

    public Shape(String shapeColor) {
        this.shapeColor = shapeColor;
    }

    public void draw() {
        System.out.print("This is " + getClass().getSimpleName() + ", color is " + shapeColor);
    }

    public abstract double calcArea();

    public String getShapeColor() {
        return shapeColor;
    }

    @Override
    public int compareTo(Object o) {
        Shape comp = (Shape)o;

        if (this.calcArea() > comp.calcArea()) {
            return 1;
        } else if (this.calcArea() < comp.calcArea()) {
            return -1;
        } else {
            return 0;
        }
    }
}
