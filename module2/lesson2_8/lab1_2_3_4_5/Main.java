package com.brainacad.module2.lesson2_8.lab1_2_3_4_5;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Shape[] myShapes = {
                new Rectangle("Red", 1, 2),
                new Rectangle("Red", 2, 3),
                new Rectangle("Red", 3, 4),
                new Rectangle("Red", 4, 5),
                new Rectangle("Red", 5, 6),
                new Circle("Green", 5),
                new Circle("Green", 10),
                new Triangle("Blue", 9, 8, 7),
                new Triangle("Blue", 6, 5, 4),
        };

        for (Shape myShape: myShapes) {
            myShape.draw();
        }
        System.out.println();

        //2.8.3
        Rectangle rect1 = new Rectangle("Black", 10, 15);
        Rectangle rect2 = new Rectangle("Black", 15, 20);
        System.out.println(rect1.compareTo(rect2));
        System.out.println();

        //2.8.4
        Rectangle[] myRectangles = {
                new Rectangle("Green", 140, 25),
                new Rectangle("Red", 12, 36),
                new Rectangle("Blue", 3, 4),
                new Rectangle("White", 4, 5),
                new Rectangle("Grey", 5, 6),
                new Rectangle("Violet", 5, 6),
        };

        Arrays.sort(myRectangles);

        for (Rectangle rect: myRectangles) {
            rect.draw();
        }
        System.out.println();

        //2.8.5
        Shape[] coloredShapes = {
                new Rectangle("Lavender", 1, 2),
                new Rectangle("Grey", 2, 3),
                new Rectangle("gold", 3, 4),
                new Rectangle("Fuchsia", 4, 5),
                new Rectangle("Cyan", 5, 6),
                new Circle("coral", 5),
                new Circle("Bisque", 10),
                new Circle("brown", 15),
                new Triangle("Aqua", 9, 8, 7),
                new Triangle("Azure", 6, 5, 4),
        };

        Arrays.sort(coloredShapes, new ShapeColorComparator());

        for (Shape coloredShape: coloredShapes) {
            System.out.println(coloredShape.getShapeColor());
        }
    }
}
