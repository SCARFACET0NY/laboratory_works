package com.brainacad.module2.lesson2_8.lab1_2_3_4_5;

public interface Drawable {
    void draw();
}
