package com.brainacad.module2.lesson2_8.lab1_2_3_4_5;

import java.util.Comparator;

public class ShapeColorComparator implements Comparator {

    @Override
    public int compare(Object o1, Object o2) {
        Shape shape1 = (Shape)o1;
        Shape shape2 = (Shape)o2;

        String str1 = shape1.getShapeColor();
        String str2 = shape2.getShapeColor();

        return str1.compareToIgnoreCase(str2);
    }
}
