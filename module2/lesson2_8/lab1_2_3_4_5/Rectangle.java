package com.brainacad.module2.lesson2_8.lab1_2_3_4_5;

public class Rectangle extends Shape {
    private double width;
    private double height;

    public Rectangle(String shapeColor, double width, double height) {
        super(shapeColor);
        this.width = width;
        this.height = height;
    }

    @Override
    public void draw() {
        super.draw();
        System.out.println(", width = " + width + ", height = " + height);
        System.out.println("Shape area is: " + String.format("%.2f", calcArea()));
    }

    @Override
    public double calcArea() {
        return width * height;
    }
}
