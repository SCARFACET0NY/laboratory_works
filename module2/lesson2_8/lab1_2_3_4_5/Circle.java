package com.brainacad.module2.lesson2_8.lab1_2_3_4_5;

public class Circle extends Shape {
    private double radius;

    public Circle(String shapeColor ,double radius) {
        super(shapeColor);
        this.radius = radius;
    }

    @Override
    public void draw() {
        super.draw();
        System.out.println(", radius = " + radius);
        System.out.println("Shape area is: " + String.format("%.2f", calcArea()));
    }

    @Override
    public double calcArea() {
        return Math.PI * radius * radius;
    }
}
