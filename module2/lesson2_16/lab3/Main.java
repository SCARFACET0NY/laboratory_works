package com.brainacad.module2.lesson2_16.lab3;

import java.util.Arrays;

public class Main {
    public static void foo(int a, int b) {
        System.out.println(a + " " + b);
    }

    public static void foo(int... a) {
        System.out.println(Arrays.toString(a));
    }

    public static void main(String[] args) {
        foo(1, 2);
        foo(1, 2, 3);
    }
}
