package com.brainacad.module2.lesson2_16.lab2;

public class Main {
    public static void foo(int i) {
        System.out.println("int");
    }

    public static void foo(Byte b) {
        System.out.println("Byte");
    }

    public static void main(String[] args) {
        byte b = 100;
        foo(b);
    }
}
