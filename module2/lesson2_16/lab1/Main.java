package com.brainacad.module2.lesson2_16.lab1;

class Animal {

}

class Dog extends Animal {

}

class Main {
    public static void foo(Animal a) {
        System.out.println("Animal");
    }

    public static void foo(Dog d) {
        System.out.println("Dog");
    }

    public static void main(String[] args) {
        Animal animal = new Animal();
        Dog dog = new Dog();
        Animal doggy = new Dog();

        foo(animal);
        foo(dog);
        foo(doggy);
    }
}