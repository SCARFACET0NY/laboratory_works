package com.brainacad.module2.lesson2_16.lab4;

class Animal {

}

class Dog extends Animal {

}

class Puppy extends Dog {

}

public class Main {
    public static void foo(Animal a) {
        System.out.println("Animal");
    }

    public static void foo(Dog d) {
        System.out.println("Dog");
    }

    public static void foo(Puppy p) {
        System.out.println("Puppy");
    }

    public static void main(String[] args) {
        foo(null);
    }
}
