package com.brainacad.module2.lesson2_7.lab6_7_8_9_10.testshapes;

public class Circle extends Shape {
    private double radius;

    public Circle(String shapeColor ,double radius) {
        super(shapeColor);
        this.radius = radius;
    }

    @Override
    public String toString() {
        return super.toString() +
                ", radius = " + radius;
    }

    @Override
    public double calcArea() {
        return Math.PI * radius * radius;
    }

}
