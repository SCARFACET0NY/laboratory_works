package com.brainacad.module2.lesson2_7.lab6_7_8_9_10.testshapes;

public class Main {
    public static void main(String[] args) {
        Shape shape = new Shape("Green");
        System.out.println(shape.toString());
        System.out.println("Shape area is: " + shape.calcArea());
        System.out.println();

        Circle circle = new Circle("Blue", 12);
        System.out.println(circle.toString());
        System.out.println("Shape area is: " + String.format("%.2f", circle.calcArea()));
        System.out.println();

        Rectangle rectangle = new Rectangle("Red", 7, 8);
        System.out.println(rectangle.toString());
        System.out.println("Shape area is: " + String.format("%.2f", rectangle.calcArea()));
        System.out.println();

        Triangle triangle = new Triangle("Yellow", 10, 15, 18);
        System.out.println(triangle.toString());
        System.out.println("Shape area is: " + String.format("%.2f", triangle.calcArea()));
        System.out.println();

        Shape[] myShapes = {
                new Rectangle("Red", 1, 2),
                new Rectangle("Red", 2, 3),
                new Rectangle("Red", 3, 4),
                new Rectangle("Red", 4, 5),
                new Rectangle("Red", 5, 6),
                new Circle("Green", 5),
                new Circle("Green", 10),
                new Triangle("Blue", 9, 8, 7),
                new Triangle("Blue", 6, 5, 4),
        };

        double sumArea = 0.0;
        double sumRectangleArea = 0.0;
        double sumCircleleArea = 0.0;
        double sumTriangleArea = 0.0;

        for (Shape myShape: myShapes) {
            System.out.println(myShape.toString());
            System.out.println("Shape area is: " + String.format("%.2f", myShape.calcArea()));

            if (myShape instanceof Rectangle) {
                sumRectangleArea += myShape.calcArea();
            } else if (myShape instanceof Circle) {
                sumCircleleArea += myShape.calcArea();
            } else if (myShape instanceof Triangle) {
                sumTriangleArea += myShape.calcArea();
            }

            sumArea += myShape.calcArea();
        }

        System.out.println();
        System.out.println("Rectangles total area: " + String.format("%.2f", sumRectangleArea));
        System.out.println("Circles total area: " + String.format("%.2f", sumCircleleArea));
        System.out.println("Triangles total area: " + String.format("%.2f", sumTriangleArea));
        System.out.println("Total area: " + String.format("%.2f", sumArea));
    }
}
