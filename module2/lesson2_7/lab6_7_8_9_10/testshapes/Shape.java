package com.brainacad.module2.lesson2_7.lab6_7_8_9_10.testshapes;

public class Shape {
    private String shapeColor;

    public Shape(String shapeColor) {
        this.shapeColor = shapeColor;
    }

    @Override
    public String toString() {
        return "This is " + getClass().getSimpleName() + ", color is " + shapeColor;
    }

    public double calcArea() {
        return 0.0;
    }
}
