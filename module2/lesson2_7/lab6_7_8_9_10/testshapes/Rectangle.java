package com.brainacad.module2.lesson2_7.lab6_7_8_9_10.testshapes;

public class Rectangle extends Shape {
    private double width;
    private double height;

    public Rectangle(String shapeColor, double width, double height) {
        super(shapeColor);
        this.width = width;
        this.height = height;
    }

    @Override
    public String toString() {
        return super.toString() + ", width = " + width + ", height = " + height;
    }

    @Override
    public double calcArea() {
        return width * height;
    }
}
