package com.brainacad.module2.lesson2_7.lab1_2_3_4_5;

public class EthernetAdapter extends Device {
    private int speed;
    private int mac;

    public EthernetAdapter(String manufacturer, float price, String serialNumber, int speed, int mac) {
        super(manufacturer, price, serialNumber);
        this.speed = speed;
        this.mac = mac;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getMac() {
        return mac;
    }

    public void setMac(int mac) {
        this.mac = mac;
    }

    @Override
    public String toString() {
        return super.toString() +
                " speed = " + speed +
                ", mac = " + mac;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        EthernetAdapter that = (EthernetAdapter) o;

        if (speed != that.speed) return false;
        return mac == that.mac;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + speed;
        result = 31 * result + mac;
        return result;
    }
}
