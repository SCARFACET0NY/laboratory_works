package com.brainacad.module2.lesson2_7.lab1_2_3_4_5;

public class Main {
    public static void main(String[] args) {
        Device[] myDevices = {
            new Device("Apple", 800.0F, "12345678"),
            new Monitor("Philips", 400.0F, "98765432", 2560, 1440),
            new EthernetAdapter("Arris", 50.0F, "23456789", 150, 20)
        };

        for (Device dev: myDevices) {
            System.out.println(dev.toString());
        }
    }
}
