package com.brainacad.module2.lesson2_1.lab1_2_3_4_5;

public class Computer {
    private String manufacturer;
    private int serialNumber;
    private float price;
    private int quantityCPU;
    private int frequencyCPU;

    public String getManufacturer() {
        return manufacturer;
    }
    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }
    public int getSerialNumber() {
        return serialNumber;
    }
    public void setSerialNumber(int serialNumber) {
        this.serialNumber = serialNumber;
    }
    public float getPrice() {
        return price;
    }
    public void setPrice(float price) {
        this.price = price;
    }
    public int getQuantityCPU() {
        return quantityCPU;
    }
    public void setQuantityCPU(int quantityCPU) {
        this.quantityCPU = quantityCPU;
    }
    public int getFrequencyCPU() {
        return frequencyCPU;
    }
    public void setFrequencyCPU(int frequancyCPU) {
        this.frequencyCPU = frequancyCPU;
    }

    public void view() {
            System.out.print(serialNumber + ", ");
            System.out.print(manufacturer + ", ");
            System.out.print(price + ", ");
            System.out.print(quantityCPU + ", ");
            System.out.println(frequencyCPU + " ");
    }

    public static void main(String[] args) {
        Computer[] arrayComp = new Computer[5];

        for (int i = 0; i < arrayComp.length; i++) {
            Computer computer = new Computer();
            computer.setSerialNumber(i + 100201601);
            computer.setManufacturer("ASUS");
            computer.setPrice(3000.5F + (int)(Math.random() * 2000));
            computer.setQuantityCPU(2 + (i % 3) * 2);
            computer.setFrequencyCPU(1600 + (int)(Math.random() * 1100));
            arrayComp[i] = computer;
        }

        for (Computer comp: arrayComp) {
            comp.view();
        }

        for (int i = 0; i < arrayComp.length; i++) {
            arrayComp[i].setPrice(arrayComp[i].getPrice() * 1.1F);
        }

        System.out.println();
        for (Computer comp: arrayComp) {
            comp.view();
        }
    }
}
