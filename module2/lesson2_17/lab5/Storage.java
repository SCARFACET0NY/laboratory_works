package com.brainacad.module2.lesson2_17.lab5;

public class Storage {
    private int count = -1;

    public int increment() {
        return count++;
    }

    public int getCount() {
        return count;
    }

    public boolean ready() {
        return getCount() < 100000 && getCount() == increment();
    }
}
