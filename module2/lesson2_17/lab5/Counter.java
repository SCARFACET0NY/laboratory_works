package com.brainacad.module2.lesson2_17.lab5;

public class Counter extends Thread {
    private Storage s;
    private int n;

    public Counter(Storage s, int n) {
        this.s = s;
        this.n = n;
    }

    @Override
    public void run() {
        try {
            sleep(1000);
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
        for (int i = 0; i <= n; i++) {
            synchronized (s) {
                s.increment();
                s.notifyAll();
            }
        }
    }

    public Storage getS() {
        return s;
    }

    public int getN() {
        return n;
    }
}
