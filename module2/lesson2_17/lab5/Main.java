package com.brainacad.module2.lesson2_17.lab5;

public class Main {
    public static void main(String[] args) {
        Storage storage = new Storage();
        Counter counter = new Counter(storage, 100000);
        Printer printer =  new Printer(storage);

        counter.start();
        printer.start();
    }
}
