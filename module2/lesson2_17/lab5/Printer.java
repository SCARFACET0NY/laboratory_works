package com.brainacad.module2.lesson2_17.lab5;

public class Printer extends Thread {
    private Storage s;

    public Printer(Storage s) {
        this.s = s;
    }

    @Override
    public void run() {
        synchronized (s) {

            while (s.ready()) {
                System.out.println(s.getCount());
            }
        }
    }
}
