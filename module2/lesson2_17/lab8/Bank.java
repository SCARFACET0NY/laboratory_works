package com.brainacad.module2.lesson2_17.lab8;

public class Bank {
    private int accounts[];
    private int numOfAccounts;

    public Bank(int numAccounts, int initBalance) {
        accounts = new int[numAccounts];
        numOfAccounts = numAccounts;

        for (int i = 0; i < accounts.length; i++) {
            accounts[i] = initBalance;
        }
    }

    void transfer(int from, int to, int amount) {
        try {
            System.out.println("from: " + from + " to: " + to + " amount: " + amount);

            if (accounts[from] < amount) return;
            accounts[from] = accounts[from] - amount;

            Thread.sleep((int)(100 * Math.random()));

            accounts[to] = accounts[to] + amount;
            System.out.println("Total balance: " + totalBalance());
        } catch (InterruptedException e) { }
    }

    public int totalBalance() {
        int total = 0;

        for (int v : accounts) {
            total = total + v;
        }

        return total;
    }

    public int getNumberAccounts() {
        return numOfAccounts;
    }
}