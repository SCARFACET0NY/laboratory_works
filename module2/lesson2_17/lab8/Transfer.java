package com.brainacad.module2.lesson2_17.lab8;

import java.util.Random;

public class Transfer extends Thread {
    private Bank bank;
    private int from;
    private int max;

    public Transfer(Bank bank, int from, int maxAmount) {
        this.bank = bank;
        this.from = from;
        max = maxAmount;
    }

    @Override
    public void run() {
        Random random = new Random();
        synchronized (bank) {
            while (true) {
                bank.transfer(random.nextInt(from), random.nextInt(from), random.nextInt(max));
            }
        }
    }
}
