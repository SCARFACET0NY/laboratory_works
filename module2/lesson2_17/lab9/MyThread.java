package com.brainacad.module2.lesson2_17.lab9;

public class MyThread extends Thread {
    private static MyObject obj1 = new MyObject();
    private static MyObject obj2 = new MyObject();
    private static MyObject obj3 = new MyObject();

    @Override
    public void run() {
        Thread t1 = new Thread1();
        Thread t2 = new Thread2();
        Thread t3 = new Thread3();
        t1.start();
        t2.start();
        t3.start();
    }

    private static class Thread1 extends Thread {
        @Override
        public void run() {
            synchronized (obj1) {
                System.out.println("Thread 1: Holding lock1...");
                try {
                    sleep(100);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
                System.out.println("Thread 1: Waiting for lock2...");
                synchronized (obj2) {
                    System.out.println("Thread 1:  Holding lock1 & lock2...");
                }
            }
        }
    }

    private static class Thread2 extends Thread {
        @Override
        public void run() {
            synchronized (obj2) {
                System.out.println("Thread 2: Holding lock2...");
                try {
                    sleep(100);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
                System.out.println("Thread 2: Waiting for lock3...");
                synchronized (obj3) {
                    System.out.println("Thread 2:  Holding lock2 & lock3...");
                }
            }
        }
    }

    private static class Thread3 extends Thread {
        @Override
        public void run() {
            synchronized (obj3) {
                System.out.println("Thread 3: Holding lock3...");
                try {
                    sleep(100);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
                System.out.println("Thread 3: Waiting for lock1...");
                synchronized (obj1) {
                    System.out.println("Thread 3:  Holding lock3 & lock1...");
                }
            }
        }
    }
}
