package com.brainacad.module2.lesson2_17.lab11;

import java.util.List;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ForkJoinPool;

public class Main {
    public static void main(String[] args) {
        final int numOfElements = 300000;
        List<Integer> list = new CopyOnWriteArrayList<>();
        Random random = new Random();

        for (int i = 0; i < numOfElements; i++) {
            list.add(random.nextInt(100));
        }

        ForkJoinPool pool = new ForkJoinPool(10);
        int computedSum = pool.invoke(new RecSumOfList(list));
        System.out.println(computedSum);

//        int sum = 0;
//        for (int i : list) {
//            sum += i;
//        }
//        System.out.println(sum);
    }
}
