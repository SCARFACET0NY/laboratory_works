package com.brainacad.module2.lesson2_17.lab11;

import java.util.List;
import java.util.concurrent.RecursiveTask;

public class RecSumOfList extends RecursiveTask<Integer> {
    List<Integer> list;

    public RecSumOfList(List<Integer> list) {
        this.list = list;
    }

    @Override
    protected Integer compute() {
        if (list.size() < 20) {
            int localSum = 0;
            for (int i : list) {
                localSum += i;
            }
            return localSum;
        } else {
            RecSumOfList firstHalf = new RecSumOfList(list.subList(0, list.size() / 2));
            int resultFirst = firstHalf.compute();
            RecSumOfList secondHalf = new RecSumOfList(list.subList(list.size() / 2, list.size()));
            int resultSecond = secondHalf.compute();
            return resultFirst + resultSecond;
        }
    }
}
