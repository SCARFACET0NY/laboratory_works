package com.brainacad.module2.lesson2_17.lab4;

import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        final int length = 1000;
        int[] myArray = new int[length];
        MySumCount count1 = new MySumCount();
        MySumCount count2 = new MySumCount();
        Thread thread0 = new Thread(count1);
        Thread thread1 = new Thread(count1);

        Random random = new Random();
        for (int i = 0; i < length; i++) {
            myArray[i] = random.nextInt(length);
        }

        count1.setValues(myArray);
        count1.setStartIndex(0);
        count1.setStopIndex(length / 2);

        count2.setValues(myArray);
        count2.setStartIndex(length / 2);
        count2.setStopIndex(length);

        thread0.start();
        thread1.start();

        try {
            thread0.join();
            thread1.join();
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }

        System.out.println(Arrays.toString(myArray));
        System.out.printf("Sum of array elements: %d", count1.getResultSum() + count2.getResultSum());
    }
}
