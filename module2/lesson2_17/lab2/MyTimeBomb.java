package com.brainacad.module2.lesson2_17.lab2;

public class MyTimeBomb implements Runnable {
    @Override
    public void run() {
        for (int i = 10; i > 0; i--) {
            System.out.println(i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
        System.out.println("Boom!!!");
    }
}

class Main {
    public static void main(String[] args) {
        MyTimeBomb bomb1 = new MyTimeBomb();
        Thread thread0 = new Thread(bomb1);
        thread0.start();
    }
}
