package com.brainacad.module2.lesson2_17.lab10_1;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Main {
    public static void main(String[] args) {
        Map<String, String> cmap = new ConcurrentHashMap<>();

        WriteConcHashMap th1 = new WriteConcHashMap("One", cmap);
        WriteConcHashMap th2 = new WriteConcHashMap("Two",cmap);
        ReadConcHashMap th3 = new ReadConcHashMap(cmap);
        ReadConcHashMap th4 = new ReadConcHashMap(cmap);
        th1.start();
        th2.start();
        th3.start();
        th4.start();


        try {
            th1.join();
            th2.join();
            th3.join();
            th4.join();
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
    }
}
