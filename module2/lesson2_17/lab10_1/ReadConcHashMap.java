package com.brainacad.module2.lesson2_17.lab10_1;

import java.util.Map;

public class ReadConcHashMap extends Thread {
    private Map<String, String> cmap;

    public ReadConcHashMap(Map<String, String> cmap) {
        this.cmap = cmap;
    }

    @Override
    public void run() {
        System.out.println(cmap);
    }
}
