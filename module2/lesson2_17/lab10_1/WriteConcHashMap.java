package com.brainacad.module2.lesson2_17.lab10_1;

import java.util.Map;

public class WriteConcHashMap extends Thread {
    private String name;
    private Map<String, String> cmap;

    public WriteConcHashMap(String name, Map<String, String> cmap) {
        this.name = name;
        this.cmap = cmap;
    }

    @Override
    public void run() {
        for (int i = 0, c = 'A'; i < 10; i++, c++) {
            cmap.put(name + String.valueOf(i), String.valueOf((char)c));
        }
        System.out.println(name + " completed");
    }
}
