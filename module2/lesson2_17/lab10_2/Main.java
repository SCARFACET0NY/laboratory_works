package com.brainacad.module2.lesson2_17.lab10_2;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();

        WriteHashMap th1 = new WriteHashMap("One", map);
        WriteHashMap th2 = new WriteHashMap("Two",map);
        ReadHashMap th3 = new ReadHashMap(map);
        ReadHashMap th4 = new ReadHashMap(map);
        th1.start();
        th2.start();
        th3.start();
        th4.start();


        try {
            th1.join();
            th2.join();
            th3.join();
            th4.join();
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
    }
}
