package com.brainacad.module2.lesson2_17.lab10_2;

import java.util.Map;

public class ReadHashMap extends Thread {
    private Map<String, String> cmap;

    public ReadHashMap(Map<String, String> cmap) {
        this.cmap = cmap;
    }

    @Override
    public void run() {
        synchronized (cmap) {
            System.out.println(cmap);
        }
    }
}
