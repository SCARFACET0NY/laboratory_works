package com.brainacad.module2.lesson2_17.lab3;

import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        final int length = 1000;
        int[] myArray = new int[length];
        MySumCount count1 = new MySumCount();
        MySumCount count2 = new MySumCount();

        Random random = new Random();
        for (int i = 0; i < length; i++) {
            myArray[i] = random.nextInt(length);
        }

        count1.setValues(myArray);
        count1.setStartIndex(0);
        count1.setStopIndex(length / 2);

        count2.setValues(myArray);
        count2.setStartIndex(length / 2);
        count2.setStopIndex(length);

        count1.start();
        count2.start();

        try {
            count1.join();
            count2.join();
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }

        System.out.println(Arrays.toString(myArray));
        System.out.printf("Sum of array elements: %d", count1.getResultSum() + count2.getResultSum());
    }
}
