package com.brainacad.module2.lesson2_17.lab7;

public class BankTest {
    public static final int N_ACCOUNTS = 5;
    public static final int INIT_BALANCE = 1000;

    public static void main(String args[]) {
        Bank bank = new Bank(N_ACCOUNTS, INIT_BALANCE);

        Transfer[] transfers = new Transfer[N_ACCOUNTS];
        for (int i = 0; i < N_ACCOUNTS; i++) {
            transfers[i] = new Transfer(bank, N_ACCOUNTS, INIT_BALANCE);
            transfers[i].start();
        }
    }
}
