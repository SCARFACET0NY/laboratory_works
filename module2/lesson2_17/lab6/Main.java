package com.brainacad.module2.lesson2_17.lab6;

public class Main {
    public static void main(String[] args) {
        DiningHall d = new DiningHall();

        for (int i = 0; i < 10; i++) {
            d.makePizza();
        }

        DiningHallThread[] dht = new DiningHallThread[20];

        for (int j = 0; j < dht.length; j++) {
            dht[j] = new DiningHallThread(d);
        }

        for (DiningHallThread dh : dht) {
            dh.start();
        }
    }
}
