package com.brainacad.module2.lesson2_17.lab6;

public class DiningHall {
    private static int pizzaNum;
    private static int studentID = 1;

    public void makePizza() {
        pizzaNum++;
    }

    public synchronized void servePizza() {
        String result;
        if (pizzaNum > 0) {
            result = "Served ";
            pizzaNum--;
        } else {
            result = "Starved ";
        }
        System.out.println(result + studentID);
        studentID++;
    }
}

class DiningHallThread extends Thread {
    private DiningHall diningHall;

    public DiningHallThread(DiningHall diningHall) {
        this.diningHall = diningHall;
    }

    @Override
    public void run() {
        diningHall.servePizza();;
    }
}
