package com.brainacad.module2.lesson2_12.lab5;

public class MyTestClass {
    private int x = 1;

    void test() {
        System.out.println("Test");
    }

    static class MyStaticNested {
        private int i = 2;

        public int getI() {
            return i;
        }

        public void setI(int i) {
            this.i = i;
        }

        public void accessOuter() {
            MyTestClass myTestClass = new MyTestClass();
            myTestClass.x = 10;
            System.out.println(myTestClass.x);
        }
    }

    class MyInner {
        private int j = 3;

        public int getJ() {
            return j;
        }

        public void setJ(int j) {
            this.j = j;
        }

        public void accessOuter() {
            x = 20;
            System.out.println(x);
        }
    }

    void localClassMethod(boolean b) {
        class MyLocal {
            private int k = 4;

            public int getK() {
                return k;
            }

            public void setK(int k) {
                this.k = k;
            }

            public void accessOuter() {
                x = 30;
                System.out.println(x);
            }
        }
        if (b) {
            System.out.println(new MyLocal().getK());
        } else {
            MyLocal myLocal = new MyLocal();
            myLocal.accessOuter();
        }
    }
}
