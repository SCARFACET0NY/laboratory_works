package com.brainacad.module2.lesson2_12.lab5;

public class Main {
    public static void main(String[] args) {
        MyTestClass.MyStaticNested nestedClass = new MyTestClass.MyStaticNested();
        MyTestClass testClass = new MyTestClass();
        MyTestClass.MyInner innerClass = testClass.new MyInner();

        System.out.println(nestedClass.getI());
        System.out.println(innerClass.getJ());
        testClass.localClassMethod(true);

        nestedClass.accessOuter();
        innerClass.accessOuter();
        testClass.localClassMethod(false);
    }
}
