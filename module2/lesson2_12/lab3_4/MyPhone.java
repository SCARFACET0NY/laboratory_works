package com.brainacad.module2.lesson2_12.lab3_4;

import java.util.Arrays;
import java.util.Comparator;

public class MyPhone {
    private MyPhoneBook phoneBook;

    public MyPhone() {
        phoneBook = new MyPhoneBook();
    }

    public void switchOn() {
        System.out.print("Loading PhoneBook records... ");

        phoneBook.addPhoneNumber("Rambo", "123-45-67");
        phoneBook.addPhoneNumber("Rico", "234-56-78");
        phoneBook.addPhoneNumber("Rocky", "345-67-89");
        phoneBook.addPhoneNumber("Ronald", "456-78-90");
        phoneBook.addPhoneNumber("Robert", "567-89-01");
        phoneBook.addPhoneNumber("Luke", "098-76-54");
        phoneBook.addPhoneNumber("Anakin", "987-65-43");
        phoneBook.addPhoneNumber("Leia", "876-54-32");
        phoneBook.addPhoneNumber("Amidala", "765-43-21");
        phoneBook.addPhoneNumber("Obi-wan", "654-32-10");

        System.out.println("OK");
    }

    public void call(int i) {
        System.out.println("Calling to: " + phoneBook.phoneNumbers[i]);
    }



    public class MyPhoneBook {
        private PhoneNumber[] phoneNumbers = new PhoneNumber[10];

        public class PhoneNumber {
            private String name;
            private String phone;

            public PhoneNumber(String name, String phone) {
                this.name = name;
                this.phone = phone;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            @Override
            public String toString() {
                return "name = '" + name + '\'' + ", phone = '" + phone + '\'';
            }
        }

        public void addPhoneNumber(String name, String phone) {
            for (int i = 0; i < phoneNumbers.length; i++) {
                if (phoneNumbers[i] == null) {
                    phoneNumbers[i] = new PhoneNumber(name, phone);
                    break;
                }
            }
        }

        public void printPhoneNumber() {
            for (int i = 0; i < phoneNumbers.length; i++) {
                if (phoneNumbers[i] != null) {
                    System.out.println(phoneNumbers[i]);
                }
            }
        }

        public void sortByName() {
            Arrays.sort(phoneNumbers, new Comparator<PhoneNumber>() {
                @Override
                public int compare(PhoneNumber o1, PhoneNumber o2) {
                    if(o1 != null && o2 != null) {
                        return o1.name.compareToIgnoreCase(o2.name);
                    }
                    return 0;
                }
            });
        }

        public void sortByPhoneNumber() {
            Arrays.sort(phoneNumbers, new Comparator<PhoneNumber>() {
                @Override
                public int compare(PhoneNumber o1, PhoneNumber o2) {
                    if(o1 != null && o2 != null) {
                        return o1.phone.compareToIgnoreCase(o2.phone);
                    }
                    return 0;
                }
            });
        }
    }

    class Camera {}

    class Bluetooth {}

    static class SimCard {}

    class PowerOnButton {}

    static class PhoneBattery {}

    static class PhoneCharger {}

    class PhoneDisplay {}

    class PhoneSpeaker{}
}

class HeadPhones {}

class MemoryCard {}