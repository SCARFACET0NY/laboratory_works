package com.brainacad.module2.lesson2_12.lab3_4;

public class Main {
    public static void main(String[] args) {
        MyPhone phone = new MyPhone();
        phone.switchOn();
        phone.call(1);
    }
}
