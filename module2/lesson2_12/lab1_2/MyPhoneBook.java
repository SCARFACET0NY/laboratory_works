package com.brainacad.module2.lesson2_12.lab1_2;

import java.util.Arrays;
import java.util.Comparator;

public class MyPhoneBook {
    private PhoneNumber[] phoneNumbers = new PhoneNumber[10];

    public static class PhoneNumber {
        private String name;
        private String phone;

        public PhoneNumber(String name, String phone) {
            this.name = name;
            this.phone = phone;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        @Override
        public String toString() {
            return "name = '" + name + '\'' + ", phone = '" + phone + '\'';
        }
    }

    public void addPhoneNumber(String name, String phone) {
        for (int i = 0; i < phoneNumbers.length; i++) {
            if (phoneNumbers[i] == null) {
                phoneNumbers[i] = new PhoneNumber(name, phone);
                break;
            }
        }
    }

    public void printPhoneNumber() {
        for (int i = 0; i < phoneNumbers.length; i++) {
            if (phoneNumbers[i] != null) {
                System.out.println(phoneNumbers[i]);
            }
        }
    }

    public void sortByName() {
        Arrays.sort(phoneNumbers, new Comparator<PhoneNumber>() {
            @Override
            public int compare(PhoneNumber o1, PhoneNumber o2) {
                if(o1 != null && o2 != null) {
                    return o1.name.compareToIgnoreCase(o2.name);
                }
                return 0;
            }
        });
    }

    public void sortByPhoneNumber() {
        Arrays.sort(phoneNumbers, new Comparator<PhoneNumber>() {
            @Override
            public int compare(PhoneNumber o1, PhoneNumber o2) {
                if(o1 != null && o2 != null) {
                    return o1.phone.compareToIgnoreCase(o2.phone);
                }
                return 0;
            }
        });
    }
}
