package com.brainacad.module2.lesson2_12.lab1_2;

public class Main {
    public static void main(String[] args) {
        MyPhoneBook myPhoneBook  = new MyPhoneBook();

        myPhoneBook.addPhoneNumber("Rambo", "123-45-67");
        myPhoneBook.addPhoneNumber("Rico", "234-56-78");
        myPhoneBook.addPhoneNumber("Rocky", "345-67-89");
        myPhoneBook.addPhoneNumber("Ronald", "456-78-90");
        myPhoneBook.addPhoneNumber("Robert", "567-89-01");
        myPhoneBook.addPhoneNumber("Luke", "098-76-54");
        myPhoneBook.addPhoneNumber("Anakin", "987-65-43");
        myPhoneBook.addPhoneNumber("Leia", "876-54-32");
        myPhoneBook.addPhoneNumber("Amidala", "765-43-21");
        myPhoneBook.addPhoneNumber("Obi-wan", "654-32-10");

        myPhoneBook.sortByName();

        myPhoneBook.printPhoneNumber();
    }
}
