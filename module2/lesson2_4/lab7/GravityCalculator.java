package com.brainacad.module2.lesson2_4.lab7;

public class GravityCalculator {
    public  static final double A = -9.81;

    public static void calcDist(double time, double velocity, int position) {
        double finalPosition = 0.5 * A * time * time + velocity * time + position;

        System.out.println("The object's position after falling for " + time + " seconds with initial velocity "
        + velocity + " and starting position " + position + " is " + finalPosition);
    }
}

class Main {
    public static void main(String[] args) {
        GravityCalculator.calcDist(10, 0, 0);
    }
}
