package com.brainacad.module2.lesson2_4.lab6;

public class MyPyramid {
    public static void printPyramid(int h) {
        if (h > 0 && h < 10) {
            for (int i = 1; i <= h; i++) {
                for (int spaces = h - i; spaces >= 0; spaces--) {
                    System.out.print(" ");
                }

                for (int j = 1; j <= i; j++) {
                    System.out.print(j);
                }

                for (int k = i - 1; k >= 1; k--) {
                    System.out.print(k);
                }

                System.out.println();
            }
        } else {
            System.out.println("Number out of range");
        }
    }
}

class Main {
    public static void main(String[] args) {
        MyPyramid.printPyramid(9);
    }
}

//for (int i = 1; i <=h; i++) {
//        System.out.printf("%" + (h + 1 - i) + "s", " ");
//        int minus = 0;
//        for (int j = 1; j < i * 2; j++) {
//            minus = j > i ? minus += 2 : 0;
//            System.out.print(j - minus);
//        }
//        System.out.println();
//}