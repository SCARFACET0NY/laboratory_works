package com.brainacad.module2.lesson2_4.lab4;

public class Employee {
    private String firstName;
    private String lastName;
    private String occupation;
    private String telephone;
    private static int numberOfEmployees;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public static int getNumberOfEmployees() {
        return numberOfEmployees;
    }

    public static void setNumberOfEmployees(int numberOfEmployees) {
        Employee.numberOfEmployees = numberOfEmployees;
    }

    public Employee(String firstName, String lastName, String occupation, String telephone) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.occupation = occupation;
        this.telephone = telephone;
        numberOfEmployees++;
    }
}

class Main {
    public static void main(String[] args) {
        Employee one = new Employee("John", "Smith", "builder", "123-45-67");
        Employee two = new Employee("Jack", "Snack", "butcher", "234-56-78");
        Employee three = new Employee("Jim", "Slim", "bouncer", "345-67-89");
        Employee four = new Employee("James", "Slide", "broker", "555-55-55");

        System.out.println(Employee.getNumberOfEmployees());
    }
}
