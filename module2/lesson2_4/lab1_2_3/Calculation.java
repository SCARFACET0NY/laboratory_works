package com.brainacad.module2.lesson2_4.lab1_2_3;

public class Calculation {
    public static void main(String[] args) {
        int[] one = {15, 17, 43, -30, -5, 28, 93, -17};
        int[] two = {4, 18, -82, - 34, 41, 12, -6, 54};

        System.out.println(MyMath.findMax(one));
        System.out.println(MyMath.findMin(two));

        MyMath.areaOfCircle(2.5);
    }
}
