package com.brainacad.module2.lesson2_4.lab1_2_3;

public class MyMath {
    public static final double PI = 3.14;

    public static int findMax (int[] values) {
        int maxValue = values[0];
        for (int element: values) {
            if (element > maxValue) {
                maxValue = element;
            }
        }

        return maxValue;
    }

    public static int findMin (int[] values) {
        int minValue = values[0];
        for (int element: values) {
            if (element < minValue) {
                minValue = element;
            }
        }

        return minValue;
    }

    public static void areaOfCircle(double radius) {
        System.out.println("The area of a circle is " + PI * radius * radius);
    }
}
