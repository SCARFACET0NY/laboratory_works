package com.brainacad.module1.lesson1_5.lab6;
import java.util.Scanner;

public class A {
    public static void main(String[] args) {
        System.out.print("Enter a positive number to calculate sum of squares of it's digits: ");
        Scanner inputScan = new Scanner(System.in);
        int userNumber = inputScan.nextInt();

        int sum = 0;
        int number = userNumber;
        while (number > 0) {
            int lastDigit = number % 10;
            sum += lastDigit * lastDigit;
            number /= 10;
        }

        System.out.println(sum);
    }
}
