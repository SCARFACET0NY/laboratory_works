package com.brainacad.module1.lesson1_5.lab1;

public class A {
    public static void main(String[] args) {
        for (int i = 1; i < 9; i++) {
            int j = i;
            while (j > 0) {
                System.out.print(j);
                j--;
            }
            System.out.print('\n');
        }
    }
}