package com.brainacad.module1.lesson1_5.lab4;

public class A {
    public static void main(String[] args) {
        int counter = 0;
        for (int i = 1; i < 301; i++) {
            if (i % 3 == 0 || i % 4 == 0) {
                System.out.println(i);
                counter++;
                if (counter == 10) {
                    break;
                }
            }
        }
    }
}
