package com.brainacad.module1.lesson1_5.lab7;
import java.util.Scanner;

public class A {
    public static void main(String[] args) {
        System.out.print("Enter a positive number to check for perfect numbers in a range (1, number): ");
        Scanner inputScan = new Scanner(System.in);
        int userNumber = inputScan.nextInt();

        int sum = 0;
        for (int i = 1; i < userNumber + 1; i++) {
            for (int j = 1; j < i; j++) {
                if (i % j == 0) {
                    sum += j;
                }
            }

            if (i == sum) {
                System.out.println(i);
            }
            sum = 0;
        }
    }
}
