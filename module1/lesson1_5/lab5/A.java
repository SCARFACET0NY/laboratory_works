package com.brainacad.module1.lesson1_5.lab5;

public class A {
    public static void main(String[] args) {
        int userNumber = 6;
        int sum = 0;
        for (int i = 1; i <= userNumber; i++) {
            sum += i;
        }

        double avg = (double)sum / userNumber;
        System.out.println(sum);
        System.out.println(avg);
    }
}
