package com.brainacad.module1.lesson1_5.lab2;
import java.util.Scanner;

public class A {
    public static void main(String[] args) {
        System.out.print("Enter a positive number that you like: ");
        Scanner inputScan = new Scanner(System.in);
        int userNumber = inputScan.nextInt();
        String textNumber;

        switch (userNumber) {
            case 1:
                textNumber = "One";
                break;
            case 2:
                textNumber = "Two";
                break;
            case 3:
                textNumber = "Three";
                break;
            case 4:
                textNumber = "Four";
                break;
            case 5:
                textNumber = "Five";
                break;
            case 6:
                textNumber = "Six";
                break;
            case 7:
                textNumber = "Seven";
                break;
            case 8:
                textNumber = "Eight";
                break;
            case 9:
                textNumber = "Nine";
                break;
            default:
                textNumber = "Other";
        }

//        if (userNumber == 1) {
//            textNumber = "One";
//        } else if (userNumber == 2) {
//            textNumber = "Two";
//        } else if (userNumber == 3) {
//            textNumber = "Three";
//        } else if (userNumber == 4) {
//            textNumber = "Four";
//        } else if (userNumber == 5) {
//            textNumber = "Five";
//        } else if (userNumber == 6) {
//            textNumber = "Six";
//        } else if (userNumber == 7) {
//            textNumber = "Seven";
//        } else if (userNumber == 8) {
//            textNumber = "Eight";
//        } else if (userNumber == 9) {
//            textNumber = "Nine";
//        } else {
//            textNumber = "Other";
//        }

        System.out.println(textNumber);
    }
}
