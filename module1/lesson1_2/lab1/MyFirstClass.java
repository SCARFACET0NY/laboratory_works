package com.brainacad.module1.lesson1_2.lab1;

public class MyFirstClass {
    public static void main(String[] args)
    {
        System.out.println(" ****     *    *       *    *");
        System.out.println("    *    * *    *     *    * *");
        System.out.println("    *   *   *    *   *    *   *");
        System.out.println("*   *  *******    * *    *******");
        System.out.println(" ***  *       *    *    *       *");
    }
}
