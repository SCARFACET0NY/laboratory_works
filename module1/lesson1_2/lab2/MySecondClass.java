package com.brainacad.module1.lesson1_2.lab2;
import java.util.Scanner;

public class MySecondClass {
    public static void main(String[] args)
    {
        System.out.print("Enter the name of the student: ");
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine();
        System.out.println(line);
        sc.close();
    }
}
