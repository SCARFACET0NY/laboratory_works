package com.brainacad.module1.lesson1_6.lab7;
import java.util.Scanner;

public class A {
    public static void main(String[] args) {
        int[][] arr =  {{1, 1, 1, 3, 4},
                        {2, 1, 3, 1, 2},
                        {2, 2, 3, 4, 1},
                        {3, 3, 3, 1, 4}};

        System.out.print("Enter a number you want to find: ");
        Scanner inputScan = new Scanner(System.in);
        int userNumber = inputScan.nextInt();

        int line = 1;
        for (int[] insideArray: arr) {
            System.out.print("Line " + line + ": [");
            line++;
            String myString = "";
            for (int i = 0; i < insideArray.length; i++) {
                if (insideArray[i] == userNumber) {
                    myString += i + ", ";
                }
            }
            System.out.print(myString.replaceAll(", $", ""));
            System.out.println("]");
        }
    }
}
