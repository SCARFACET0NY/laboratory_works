package com.brainacad.module1.lesson1_6.lab2;
import java.util.Arrays;

public class A {
    public static void main(String[] args) {
        int[] m = new int[] { 10, 21, 5, 22, 9, 29, 25, 22, 11, 14, 8, 14 };

        int maxValue = m[0];
        for (int element: m) {
            if (element > maxValue) {
                maxValue = element;
            }
        }

        int minValue = Integer.MAX_VALUE;
        for (int element: m) {
            if (element < minValue) {
                minValue = element;
            }
        }

        int sum = 0;
        for (int element: m) {
            sum += element;
        }

        double avgValue = (double)sum / m.length;

        System.out.println(Arrays.toString(m));
        System.out.println("Max = " + maxValue);
        System.out.println("Min = " + minValue);
        System.out.println("Avg = " + avgValue);
    }
}
