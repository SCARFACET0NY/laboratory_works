package com.brainacad.module1.lesson1_6.lab6;

import java.util.Arrays;

public class A {
    public static void main(String[] args) {
        int[] temperature = new int[12];
        for (int i = 0; i < temperature.length; i++) {
            temperature[i] = (int)(Math.random() * 100);
            if (i % 3 == 0) {
                temperature[i] = -temperature[i];
            }
        }
        int i = 0;
        int j = temperature.length - 1;
        while (i < j) {
            while (temperature[i] < 0 && i < j) {
                i++;
            }
            while (temperature[j] > 0 && j > i) {
                j--;
            }

            int temp = temperature[i];
            temperature[i] = temperature[j];
            temperature[j] = temp;
        }
        System.out.println("Initial -> " + Arrays.toString(temperature));
    }
}
//for (int i = arr.length - 1; i > 0; i--) {
//    for (int j = 0; j < i; j++) {
//        if(arr[j] > arr[j + 1]) {
//            int tmp = arr[j];
//            arr[j] = arr[j + 1];
//            arr[j + 1] = tmp;
//        }
//    }
//}