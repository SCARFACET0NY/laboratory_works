package com.brainacad.module1.lesson1_6.lab3;

public class A {
    public static void main(String[] args) {
        int numElements = 4;
        int[][] arr1 = new int[numElements][numElements];

        int num = 1;
        for (int i = 0; i < numElements; i++) {
            for (int j = 0; j < numElements; j++) {
                arr1[j][i] = num;
                num ++;
            }
        }

        for (int[] firstArray: arr1) {
            for (int element: firstArray) {
                System.out.print(element + "\t");
            }
            System.out.println();
        }
    }
}
