package com.brainacad.module1.lesson1_6.lab1;
import java.util.Arrays;

public class A {
    public static void main(String[] args) {
        final int MAX_NUMBER = 30;
        int size = MAX_NUMBER / 2;
        int[] myArray = new int[size];

        for (int i = 0, number = 2; i < myArray.length; i++, number += 2) {
            myArray[i] = number;
        }

        System.out.println(Arrays.toString(myArray));
    }
}
