package com.brainacad.module1.lesson1_6.lab5;

import java.util.Arrays;

public class A {
    public static void main(String[] args) {
        int[][] arr =  {{1, 2, 3, 4},
                        {5, 6, 7, 8},
                        {9, 10, 11, 12},
                        {13, 14, 15, 16}};

        int temp;
        for (int i = 1; i < arr.length; i++) {
            for (int j = 0; j < i; j++) {
                temp = arr[i][j];
                arr[i][j] = arr[j][i];
                arr[j][i] = temp;

            }
        }

        for (int[] firstArray: arr) {
            System.out.println(Arrays.toString(firstArray));
        }
    }
}
