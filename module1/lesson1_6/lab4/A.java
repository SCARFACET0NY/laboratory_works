package com.brainacad.module1.lesson1_6.lab4;
import java.util.Arrays;

public class A {
    public static void main(String[] args) {
        int[] myArray = {5, 23, 67, 8, 0, 34, 18, 22};
        Arrays.sort(myArray);

        int searchValue = 67;
        int retValue = Arrays.binarySearch(myArray, searchValue);

        System.out.println("Index of element " + searchValue + " is " + retValue);
    }
}
