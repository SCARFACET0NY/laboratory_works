package com.brainacad.module1.lesson1_4.lab6;

public class A {
    public static void main(String[] args) {
        int a = 7;                  // 00000000 00000000 00000000 00000111
        int b = 22;                 // 00000000 00000000 00000000 00010110
        System.out.println(a & b);  // 00000000 00000000 00000000 00000110
        System.out.println(a | b);  // 00000000 00000000 00000000 00010111
        System.out.println(a ^ b);  // 00000000 00000000 00000000 00010001
        System.out.println(~a);     // 00000000 00000000 00000000 11111000
    }
}