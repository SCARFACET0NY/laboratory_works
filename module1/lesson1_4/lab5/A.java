package com.brainacad.module1.lesson1_4.lab5;

public class A {
    public static void main(String[] args) {
        int a = 104;                 // 00000000 00000000 00000000 01101000
        int b = 104;                 // 00000000 00000000 00000000 01101000
        int c = -5;                  // 11111111 11111111 11111111 11111011
        System.out.println(a >> 2);  // 00000000 00000000 00000000 00011010
        System.out.println(b / 4);   // 00000000 00000000 00000000 00011010
        System.out.println(a << 3);  // 00000000 00000000 00000011 01000000
        System.out.println(b * 8);   // 00000000 00000000 00000011 01000000
        System.out.println(c >>> 2); // 00111111 11111111 11111111 11111110
    }
}
