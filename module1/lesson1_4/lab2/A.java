package com.brainacad.module1.lesson1_4.lab2;

public class A {
    public static void main(String[] args) {
        float a = 115.7F;
        int b = 82;
        System.out.println(a + b);
        System.out.println(a - b);
        System.out.println(a * b);
        System.out.println(a / b);
        System.out.println(a % b);
        System.out.println(678 / b);
    }
}
