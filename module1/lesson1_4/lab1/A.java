package com.brainacad.module1.lesson1_4.lab1;

public class A {
    public static void main(String[] args)
    {
        byte a = 15;
        short b = 32000;
        int c = 2_000_000_000;
        long d = 123_456_789_012L;
        float e = 54.902F;
        double f = 43534.8946040;
        boolean g = true;
        char h = 'b',
             h_1 = '\u002e';
        System.out.println("Byte -> " + a);
        System.out.println("Short -> " + b);
        System.out.println("Int -> " + c);
        System.out.println("Long -> " + d);
        System.out.println("Float -> " + e);
        System.out.println("Double -> " + f);
        System.out.println("Boolean -> " + g);
        System.out.println("Char -> " + h);
        System.out.println("Char -> " + h_1);
    }
}
