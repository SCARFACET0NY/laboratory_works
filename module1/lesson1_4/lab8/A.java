package com.brainacad.module1.lesson1_4.lab8;

public class A {
    public static void main(String[] args) {
        byte a = 5;
        int b = 70000;
        double c = 1.3453465564215;
        char d = 'в';

        int a_c = (int)a;
        byte b_c = (byte)b;
        float c_c = (float)c;
        int d_c = (int)d;
        char e_c = (char)b;
        byte f_c = (byte)(a + 300);

        System.out.println(a_c);
        System.out.println(b_c);
        System.out.println(c_c);
        System.out.println(d_c);
        System.out.println(e_c);
        System.out.println(f_c);
    }
}
