package com.brainacad.module1.lesson1_4.lab3;

public class A {
    public static void main(String[] args) {
        boolean a = true;
        boolean b = false;
        int countLong = 0;
        int countShort = 0;

        System.out.println(b & ++countLong == 0);
        System.out.println(countLong);
        System.out.println(b && ++countShort == 0 );
        System.out.println(countShort);

        System.out.println(a | ++countLong == 0);
        System.out.println(countLong);
        System.out.println(a || ++countLong == 0);
        System.out.println(countShort);

        System.out.println(a ^ b);
        System.out.println(!a);
    }
}
