package com.brainacad.module1.lesson1_4.lab4;

public class A {
    public static void main(String[] args) {
        int a = 10;
        int b = 8;
        //System.out.println(a++);
        //System.out.println(++a);
        System.out.println(--b);
        System.out.println(b--);
        System.out.println(a++ + --a - a--);
        System.out.println(a);
    }
}
